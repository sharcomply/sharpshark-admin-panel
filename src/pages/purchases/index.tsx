import React from "react";
import {PurchasesList} from "../../features/Purchases/components/PurchasesList";

export const PurchasesListPage = () => {
    return (<React.Fragment>
        <PurchasesList/>
    </React.Fragment>)
}