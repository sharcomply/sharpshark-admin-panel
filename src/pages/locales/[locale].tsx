import { useParams } from "react-router-dom";
import { LoadingOverlay, Stack } from "@mantine/core";
import { LocalView } from "features/Locale";
import { useLocale } from "features/Locale/hooks/useLocale";
import { Page } from "../../components/Page";
import { useBaseLocale } from "features/Locale/hooks/useBaseLocale";

export function LocalePage() {
    const { localeName } = useParams();
    const { locale, isLoading } = useLocale(localeName);
    const { isLoading: isLoadingBase } = useBaseLocale();

    if (isLoading || isLoadingBase)
        return (
            <Page>
                <Stack sx={{ position: "relative", flex: 1, minHeight: 180 }} px={20} py={40}>
                    <LoadingOverlay overlayColor={"transparent"} visible={true} />
                </Stack>
            </Page>
        );

    if (!locale) return <Page>locale not found</Page>;

    return (
        <Page>
            <LocalView localeName={localeName} />
        </Page>
    );
}
