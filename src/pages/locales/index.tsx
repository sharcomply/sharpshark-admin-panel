import { LocaleList } from "features/Locale/components/LocaleList";
import React from "react";

export function LocalesListPage() {

    return (
        <React.Fragment>
            <LocaleList/>
        </React.Fragment>
    )
}
