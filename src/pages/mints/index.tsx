import React from "react";
import {MintList} from "../../features/Mints/components/MintsList";

export function MintsListPage() {

    return (
        <React.Fragment>
            <MintList/>
        </React.Fragment>
    )
}

