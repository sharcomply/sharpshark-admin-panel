import DisputesList from "features/DisputesList";

const DisputListPage = () => {
    return (
        <>
            <DisputesList />
        </>
    );
};

export default DisputListPage;
