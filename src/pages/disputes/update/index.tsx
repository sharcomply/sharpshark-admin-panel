import styled from "@emotion/styled";
import {Group, Stack} from "@mantine/core";
import PageTitle from "components/PageTitle";
import {SharkButton} from "components/SharkButton";
import {TabLink} from "components/TabLink";
import DisputForms from "features/DisputForms";
import {useDisputDetail} from "features/DisputForms/hooks/useDisputDetail";
import {useDisputeUpdate} from "features/DisputForms/hooks/useDisputeUpdate";
import {useCallback, useEffect} from "react";
import {useForm} from "react-hook-form";
import {useParams} from "react-router-dom";


const tabNotAllowed = ["Closed", "Claim Sent: Google"]

const DisputUpdatedPage = () => {
  const {disputId} = useParams();
  const {disput} = useDisputDetail(disputId!);

  const {control, register, handleSubmit, getValues, reset} = useForm({
    defaultValues: disput,
  });

  useEffect(() => {
    reset(disput);
  }, [reset, disput]);

  const {handleUpdate, loading} = useDisputeUpdate();

  const onSubmit = useCallback(
    (values: any) => {
      handleUpdate(values);
    },
    [handleUpdate]
  );

  if (!disput) return null;
  return (
    <Root>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Stack>
          <div style={{display: "flex", justifyContent: "space-between"}}>
            <PageTitle title={`Dispute resolution: ${getValues("status" as any)} `}/>
            <SharkButton type="submit" buttonType="secondary" variant="outlined" loading={loading} disabled={loading}>
              Save & close
            </SharkButton>
          </div>
          <Group position={"apart"} spacing={16}>
            <Group spacing={12}>
              <TabLink variant="outlined" to="collecting-evidence" label="Collecting Evidence"/>
              <TabLink variant="outlined" to="generating-claim" label="Generating Claim"/>
              <TabLink variant="outlined" to="sending" label="Sending"/>
              {!tabNotAllowed.includes(disput.status!) &&
                  <TabLink variant="outlined" to="notify-users" label="Notify Users"/>
              }
            </Group>
          </Group>

          <DisputForms getValues={getValues} control={control} register={register}/>
        </Stack>
      </form>
    </Root>
  );
};

export default DisputUpdatedPage;

const Root = styled.div`
  background-color: #ffffff;
  border: 1px solid #dddeee;
  border-radius: 8px;
  margin: 40px;
  padding: 20px;
  margin-top: 65px;
`;

