import { Divider, Paper, Stack, useMantineTheme } from "@mantine/core";
import { LogoLarge } from "components/Icons";
import { LoginForm } from "features/Login";

export function LoginPage() {
    const theme = useMantineTheme();

    return (
        <Paper sx={{ width: 420 }} radius={"md"} p={"xl"} withBorder>
            <Stack my={"md"} justify={"center"}>
                <LogoLarge height={32} />
            </Stack>

            <Divider label="login as admin" labelPosition="center" my="lg" color={theme.colors.grayscale[5]} />

            <LoginForm />
        </Paper>
    );
}
