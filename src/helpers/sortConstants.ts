export const sortsTypes = {
    date: [
        {label: 'Asc', value: 'a-z'},
        {label: 'Desc', value: "z-a"}
    ],
    number: [
        {label: "1-9", value: "a-z"},
        {label: "9-1", value: "z-a"},
    ],
    string: [
        {label: "A-z", value: "a-z"},
        {label: "Z-a", value: "z-a"},
    ]
}