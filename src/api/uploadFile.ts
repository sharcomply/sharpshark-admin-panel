import {api} from "../index";
import axios from "axios";

export const getFileUploadLinkEndpoint = (fileName: string) =>
    `https://corp.sharpshark.io/api/v1/admin/get-locale-file-upload-link?name=${fileName}`;

export function uploadFiles<T extends Record<string, File | string>>(files: T) {
    const responses: Record<keyof T, string> = {} as Record<keyof T, string>;

    return Promise.all(
        Object.entries(files).map(
            async ([name, file]: [keyof T, File | string]) => {
                return new Promise<void>((resolve, reject) => {
                    if (typeof file === "string") {
                        responses[name] = file;
                        return resolve();
                    }

                    api
                        .get<{ uploadLink: any; downloadLink: any }>(
                            getFileUploadLinkEndpoint(file.name)
                        )
                        .then((response:any) => {
                            console.log(response)
                            if (
                                response.data &&
                                response.data.data.uploadLink &&
                                response.data.data.downloadLink
                            ) {

                                const data = new FormData();
                                const fields = response.data.data.uploadLink.fields || {};
                                Object.keys(fields).forEach((key) => {
                                    data.append(key, fields[key]);
                                });
                                data.append("file", file);

                                console.log(data)

                                axios
                                    .create()
                                    .post(response.data.data.uploadLink.url, data)
                                    .then((result) => {
                                        console.log(result)
                                        responses[name] = response.data.data.downloadLink;
                                        resolve();
                                    });
                            } else {
                                reject();
                            }
                        });
                });
            }
        )
    ).then(() => responses);
}
