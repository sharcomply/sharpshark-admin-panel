import { api } from "../index";
import { CreateLocaleForm, LocaleItemResponseType, LocaleItemType, LocaleUploadLinkType, UpdateLocaleParams } from "../types/locale";
import axios from "axios";
import { uploadFiles } from "./uploadFile";

const getBaseLocale = async () => {
    const baseLocaleUrl = "https://d1k1dlls6f6nna.cloudfront.net/locales/base.json";
    const data = await fetchFile(baseLocaleUrl);
    return data;
};

export async function fetchLocales() {
    try {
        const response = await api.get<ApiResponseList<LocaleItemType>>(`/admin/get-locales`);
        return response.data.data;
    } catch (e: any) {
        console.log("error");
    }
}

export async function createLocaleRequest(form: CreateLocaleForm) {
    try {
        const response = await api.post<ApiResponse<LocaleItemResponseType>>("/admin/create-locale", form);
        return response.data.data;
    } catch (e) {
        console.log(e);
    }
}

export async function fetchLocalesLink(name: string) {
    try {
        const response = await api.get<ApiResponse<LocaleUploadLinkType>>(`/admin/get-locale-file-upload-link?name=${name}`);
        return response.data.data;
    } catch (e: any) {
        console.log("error");
    }
}

export async function updateLocaleRequest(data: LocaleItemType) {
    if (!data._id) return;
    try {
        const response = await api.patch<ApiResponse<LocaleItemResponseType>>(`/admin/update-locale/${data?._id}`, { name: data.name, link: data.link });
        return response.data.data;
    } catch (e) {
        console.log(e);
    }
}

export async function updateLocale({ json, locale }: UpdateLocaleParams) {
    if (!json) return;

    const blob = new Blob([JSON.stringify(json, null, 2)], { type: "application/json" });
    const file = new File([blob], `${locale.name}.json`);

    try {
        const uploadFilePromise = await uploadFiles({ value: file });
        const link = uploadFilePromise.value;
        if (!link) return;
        locale.link = link;
        await updateLocaleRequest(locale);
    } catch (e) {
        console.log(e);
    }
}

export async function createLocale(name: string) {
    const jsonBase = await getBaseLocale();

    if (!jsonBase) return;

    const blob = new Blob([JSON.stringify(jsonBase, null, 2)], { type: "application/json" });
    const file = new File([blob], `${name}.json`);

    try {
        const uploadFilePromise = await uploadFiles({ value: file });
        const link = uploadFilePromise.value;
        console.log(link);
        if (!link) return console.log(`can't find link`);

        await createLocaleRequest({ name, link }).then((res) => res);
    } catch (e) {
        console.log(e);
    }
    return;
}

export const fetchFile = async (url?: string) => {
    if (!url) return;
    const response = await axios.get(url);
    return response.data;
};

export const deleteLocationRequest = async (id: string) => {
    try {
        const response = await api.delete<ApiResponse<LocaleItemType>>(`/admin/delete-locale/${id}`);
        return response.data.data;
    } catch (e) {
        console.log(e);
    }
};
