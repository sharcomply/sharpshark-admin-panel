import { api } from "index";

const AuthCodes = {
    404: "User not found",
    401: "Invalid credentials",
}

function errorHandler(namespace: string, e: any) {
    const code = e.response?.statusCode as keyof typeof AuthCodes
        
    if (AuthCodes.hasOwnProperty(code)) {
        throw AuthCodes[code]
    }
    
    console.error(namespace, e.response)
}


// Requests
export async function login(json: LoginParams) {
    try {
        const response = await api.post<ApiResponse<LoginPayload>>(`/public/auth-admin`, json)
        return response.data.data.authToken
    } catch(e: any) {
        errorHandler('[LOGIN]', e)
    }
}

export async function checkAuth() {
    try {
        const response = await api.get<ApiResponse<any>>(`/user/whoami`)
        return response.data.data
    } catch(e: any) {
        errorHandler('[CHECK_AUTH]', e)
    }
}
