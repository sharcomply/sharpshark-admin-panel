import axios from "axios";
import { getToken, removeToken } from "helpers/tokenStorage";

import * as auth from "./auth";
import * as locales from "./locales";
import * as disputes from "./disputes"

export const createApi = () => {
    const instance = axios.create({
        baseURL: "https://corp.sharpshark.io/api/v1/",
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
            'Access-Control-Allow-Credentials': true
        }
    });

    instance.interceptors.request.use((config) => {
        const token = getToken();
        if (token) {
            (config.headers as any).common["Authorization"] = `${token}`;
        }

        return config;
    });

    instance.interceptors.response.use(res => res, error => {
        if (error.response.status === 401) {
            removeToken();
            (window as Window).location = '/auth/login';
        }
        return error;
    });

    return instance;
};

export const backend = {
    auth,
    locales,
    disputes
};
