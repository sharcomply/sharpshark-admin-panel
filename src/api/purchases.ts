import {api} from "../index";
import {PurchasesItemType, UpdatePurchaseForm} from "../types/purchases";


export async function fetchPurchases(skip: number, search: string, sortOptions: any[], status: string) {
    try {
        const response = await api.get<ApiResponseList<PurchasesItemType>>(`/admin/get-purchases?skip=${skip}&limit=15&search=${search}&sort=${JSON.stringify(sortOptions)}&${status ? `status=${status}` : ""}`)
        return response.data.data
    } catch (e: any) {
        console.log(e)
    }
}

export async function updatePurchaseRequest (id: string, form:UpdatePurchaseForm) {
    try {
        const response = await api.patch<ApiResponse<PurchasesItemType>>(`/admin/update-purchase/${id}`, form)
        return response.data.data
    }catch (e) {
        console.log(e)
    }
}