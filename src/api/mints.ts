import {api} from "../index";
import {MintItemType} from "../types/mint";

export async function fetchMints(skip: number, search: string, sortOptions: any[]) {

    try {
        const response = await api.get<ApiResponseList<MintItemType>>(`/admin/get-mints?skip=${skip}&limit=15&search=${search}&sort=${JSON.stringify(sortOptions)}`)
        return response.data.data
    } catch (e: any) {
        console.log(e)
    }
}