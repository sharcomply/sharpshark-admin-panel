import axios from "axios"
import {api} from "index"

export async function fetchDisputesList(status: string, skip: number, search: string, sortOptions: any[]) {
    try {
        const sort = !!sortOptions.length ? JSON.stringify(sortOptions) : ""
        const response = await api.get<ApiResponseList<Disput>>(`admin/get-disputes?status=${status}&skip=${skip}&limit=15&search=${search}&sort=${sort}`)
        return response.data.data
    } catch (e: any) {
        console.log(e)
    }
}

export async function fetchDisputDetail(id: string) {
    try {
        const response = await api.get<ApiResponse<Disput>>(`admin/get-dispute/${id}`)
        return response.data.data
    } catch (e: any) {
        console.log(e)
    }
}

export const editDisput = async (params: EditDisput) => {
    const json = {
        _id: params._id,
        alertId: params.alertId,
        appearedOn: params.appearedOn,
        demands: params.demands,
        payAmount: params.payAmount,
        publicationQuestionLinks: params.publicationQuestionLinks,
        publicationQuestionWebarchiveLinks: params.publicationQuestionWebarchiveLinks,
        isPublicationQuestionWebarchiveFailed: params.isPublicationQuestionWebarchiveFailed,
        publicationLinks: params.publicationLinks,
        publicationWebarchiveLinks: params.publicationWebarchiveLinks,
        isPublicationWebarchiveFailed: params.isPublicationWebarchiveFailed,
        draftsScreenshots: params.draftsScreenshots,
        publicationQuestionScreenshots: params.publicationQuestionScreenshots,
        ownerClaimEmails: params.ownerClaimEmails,
        providerClaimEmails: params.providerClaimEmails,
        status: params.status
    };

    const response = await api.post<ApiResponse<Disput>>(`admin/edit-dispute/${params._id}`, json)

    return response.data.data
};

export const findEmails = async (id: string) => {

    const response = await api.post<ApiResponse<FindEmails>>(`admin/find-emails-dispute/${id}`)

    return response.data.data
};

export const sendEmails = async (id: string) => {

    const response = await api.post<ApiResponse<any>>(`admin/send-emails-dispute/${id}`)

    return response.data.data
};


export const getImageUploadLink = async (id: string) => {
    const response = await api.get<ApiResponse<GetUploadLink>>(`admin/get-image-upload-link/${id}?fileExt=png`)

    return {
        uploadLink: response.data.data.uploadLink,
        downloadLink: response.data.data.downloadLink,
    };
};

export const uploadFile = async (url: string, file: File, fields: any) => {
    if (!file) return;

    const body = new FormData();
    Object.keys(fields).forEach((key) => {
        body.append(key, fields[key]);
    });
    body.append("file", file);

    return await axios.post(url, body)
};

