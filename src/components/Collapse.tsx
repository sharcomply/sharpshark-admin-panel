import {ReactNode, useState} from "react";
import {Collapse as CollapseRoot, createStyles, Group, Stack, Text} from "@mantine/core"
import {Icon} from "components/Icon";


const useStyles = createStyles({
    button: {
        cursor: "pointer",

        '&': {
            transitionDuration: "150ms",
            transitionProperty: "background-color",
            transitionTimingFunction: "linear",
        },

        "&:hover": {
            backgroundColor: "rgb(248, 249, 250)",
        },

        "&.opened": {
            backgroundColor: "rgb(248, 249, 250)",
        }
    }
})

// Props
type CollapseProps = {
    initialOpen?: boolean;
    label?: string;
    children?: ReactNode;
}

export const Collapse = ({initialOpen = false, label, children}: CollapseProps) => {
    const [isOpen, setOpen] = useState(initialOpen);
    const {classes} = useStyles()

    return (
        <Stack spacing={0} sx={{
            "&:not(:first-child)": {
                borderTop: '1px solid #DDDEEE'
            }}}>
            <Group className={classes.button} onClick={() => setOpen((v) => !v)} px={16} py={12}>
                <Icon icon={`ic:round-keyboard-arrow-down`} color="#4b506d"/>
                <Text size="md" weight={'500'} color="#4b506d">{label}</Text>
            </Group>
            <CollapseRoot in={isOpen}>
                <Stack spacing={8} p={16} pt={8}>
                    {children}
                </Stack>
            </CollapseRoot>
        </Stack>
    )
}