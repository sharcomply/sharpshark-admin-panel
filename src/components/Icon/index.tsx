import { Icon as Iconify, IconProps as IconifyProps } from "@iconify/react";
import { Box, BoxProps, createStyles } from "@mantine/core";
import React, { PropsWithChildren, ReactNode } from "react";

// Props
export interface IconProps extends PropsWithChildren<Omit<IconifyProps, "icon">> {
    icon: string;
    color?: string;
    hoverColor?: string;
    size?: number;
    rotate?: number;
    container?: ReactNode;
}

// Styles
const useStyles = createStyles((_, { size, color = "#ffffff", hoverColor, rotate = 0 }: IconProps) => ({
    root: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        cursor: "pointer",
        fontSize: `${size || 18}px`,
        color: color,
        transform: `rotate(${rotate}deg)`,

        "& > *": {
            display: "flex",
        },

        "&:hover": {
            color: hoverColor || color,
            transition: "color 0.2s ease-in-out",
        },
    },
}));

// Component
export function Icon(props: IconProps & BoxProps<"div">) {
    const { icon, color = "#ffffff", hoverColor, size = 18, container, rotate, ...iconProps } = props;

    const { classes } = useStyles({ icon, color, hoverColor, size, rotate });

    const childrenWithProps = React.Children.map(container, (child) => {
        if (!React.isValidElement(child)) return child;

        return React.cloneElement(child, { children: <Iconify {...iconProps} icon={icon} /> });
    });

    return <Box className={classes.root}>{childrenWithProps || <Iconify {...iconProps} icon={icon} />}</Box>;
}
