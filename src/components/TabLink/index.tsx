import { css } from "@emotion/react";
import styled from "@emotion/styled";
import { Group } from "@mantine/core";
import clsx from "clsx";

import { NavLink } from "react-router-dom";

// Props
type ComponentProps = {
    label: string;
    disabled?: boolean;
    variant: 'filled' | 'outlined';
    to: string;
}

// Component
export function TabLink(props: ComponentProps) {
    const { label, disabled, variant = 'outlined', to } = props;

    const handleClick = (e: any) => {
        if (!disabled) return
        e.preventDefault()
    }

    return (
        <NavLink to={to} onClick={handleClick}>
            {({ isActive }) => (
                <Root className={clsx([isActive && 'active', disabled && 'disabled'])} variant={variant}>
                    <Group spacing={16} sx={{ position: 'relative', justifyContent: 'center' }}>
                        {label}
                    </Group>
                </Root>
            )}
        </NavLink>
    )
}

//  Styling
const Root = styled(Group)<Pick<ComponentProps, 'disabled' | 'variant'>>`
    cursor: pointer;
    border-radius: 4px;
    font-size: 12px;
    line-height: 16px;
    font-weight: 500;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 4px 12px;
    text-transform: uppercase;

    &.disabled {
        pointer-events: none;
    }

    & {
        transition-timing-function: ease-in-out;
        transition-duration: 150ms;
        transition-property: background-color, color, border, box-shadow;
    }

    ${({ variant }) => !!(variant === "filled") && FilledTab};
    ${({ variant }) => !!(variant === "outlined") && OutlinedTab};;
`

const FilledTab = css`
    background-color: #0A001E;
    color: #ffffff;

    &:hover {
        background-color: #1c00a6;
        color: #ffffff;
    }

    &:active {
        background-color: #666892;
        color: #ffffff;
    }

    &.active {
        background-color: #666892;
        color: #ffffff;
    }

    &.disabled {
        background-color: #F0F1F7;
        color: #979CBC;
    }
`;

const OutlinedTab = css`
    background-color: #FFFFFF;
    color: #666892;
    border: 0.5px solid #DDDEEE;

    &:hover {
        background-color: #FFFFFF;
        color: #1C00A6;
        border: 0.5px solid #1C00A6;
    }

    &:active {
        background-color: #666892;
        color: #ffffff;
        border: 0.5px solid transparent;
    }

    &.active {
        background-color: #666892;
        color: #ffffff;
        border: 0.5px solid transparent;
    }

    &.disabled {
        background-color: #F0F1F7;
        color: #979CBC;
        border: 0.5px solid transparent;
    }
`;
