import { Navigate, Params, useParams } from "react-router-dom";

// Component Props
type ComponentProps = {
    to: (params: Params<string>) => string;
};

export const Redirect = ({ to }: ComponentProps) => {
    const params = useParams();
    const path = to(params);
    
    return <Navigate to={path} replace />;
};
