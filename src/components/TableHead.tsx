import {createStyles, Select, Text} from '@mantine/core';
import {useCallback, useEffect, useState} from "react";

type TitleTable = {
    title: string;
    width?: string;
    value?: boolean;
    field?: string;
    sortType?: any
}


type ComponentProps = {
    items?: TitleTable[],
    change?: (values: any) => void,
    sortOptions?: any,
    filters?: any
    changeFilters?: (filtres: any) => void
}

export const TableHead = ({items = [], change, filters, changeFilters}: ComponentProps) => {
    const {classes} = useStyles()
    const [sort, setSort] = useState<any[]>([])

    const handleChange = useCallback((value: string | null, item: TitleTable) => {
        console.log(value)
        if (value === null) {
            const newValues = sort
            const sortOption = sort.find((sortItem) => sortItem.name === item.field)
            const index = sort.indexOf(sortOption)
            newValues.splice(index, 1)
            setSort(newValues)
            if (change)
                change(newValues)

        } else {
            const sortOption = sort.find((sortItem) => sortItem.name === item.field)
            if (!sortOption) {
                const newValues = sort
                const sortOption = {
                    name: item.field,
                    value: value === "a-z" ? true : false
                }
                newValues.push(sortOption)
                setSort(newValues)
                if (change)
                    change(newValues)
            } else {
                const index = sort.indexOf(sortOption)
                const newValues = sort
                newValues[index].value = value === "a-z" ? true : false
                setSort(newValues)
                if (change)
                    change(newValues)
            }

        }

        // item.value = !item.value
    }, [setSort, sort, change])

    useEffect(() => {
        console.log(filters)
    }, [filters])

    const changeFilter = useCallback((v:string | null, filter:any) => {
        if(!v) {
            if(changeFilters)
            changeFilters([])
        } else {
            const value = {
                name: filter.name,
                value: v
            }
            if(changeFilters)
            changeFilters(value)
        }

    }, [changeFilters])

    const renderFilters = (item:TitleTable) => {
        const filter = filters.find((filter:any) => filter.name === item.title.toLowerCase())
        if(!filter) return
        return (
            <Select ml={12} placeholder={'Filter'} onChange={(v) => changeFilter(v, filter)} allowDeselect style={{width: 140}}
                    data={filter.items}/>
        )
    }



    const renderItems = () => items.map((item, index) => (
        <th className={`${classes.th} ${item.field ? classes.sortable : ""}`} key={index}
            style={{width: item.width || 'auto', alignItems: "center", justifyContent: "center"}}>
            <Text
                sx={{fontSize: 12, lineHeight: '16px'}}
                weight={'500'}
                transform={'uppercase'}
                style={{alignItems: 'center', display: 'flex'}}
                color={'#0A001E'}
            >
                {item.title}
                {item.field &&
                    <Select ml={12} placeholder={"Sort"} onChange={(v) => handleChange(v, item)} allowDeselect style={{width: 80}}
                            data={item.sortType}/>
                }
                { filters && filters.find((filter: any) => filter.name === item.title.toLowerCase()) && renderFilters(item)}
            </Text>
        </th>
    ));

    return (
        <thead className={classes.thead}>
        <tr>
            {renderItems()}
        </tr>
        </thead>
    );
};

const useStyles = createStyles(() => ({
    thead: {
        fontWeight: 'bold',
        color: '#0a001e',
    },

    th: {
        verticalAlign: 'bottom',
        border: 'none !important',
        textAlign: 'start',
        padding: '16px'
    },
    sortable: {
        cursor: "pointer"
    }
}))
