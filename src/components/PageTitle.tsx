import {createStyles, Group, Text} from "@mantine/core";
import {useWizzardFormStore} from "../features/DisputForms/hooks/useWizzardFormStore";

// Props
interface PageTitleProps {
    title?: string;
}

const useStyles = createStyles(() => ({
    root: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        height: "42px",
        flexShrink: 0,
        marginBottom: "10px",
    },
    leftSide: {
        display: "flex",

        "& > *": {
            display: "flex",
            marginRight: "8px",
        },
    },
    title: {
        overflow: "hidden",
        textOverflow: "ellipsis",
        whiteSpace: "nowrap",
        fontSize: 24,
        fontWeight: 700,
        lineHeight: 30,
        color: "#0a001e",
    },
}));

// Component
export default function PageTitle(props: PageTitleProps) {
    const { classes } = useStyles();
    const { title } = props;
    const { status } = useWizzardFormStore();
    const badgeColor = status.type === "error" ? "#E50C54" : status.type === "success" ? "#29CCB1" : status.type === "warn" ? "#FFC933" : "#979CBC";
    const badgeTextColor = status.type === "error" ? "#FFFFFF" : !status.type ? "#FFFFFF" : "#0A001E";

    return (
        <div className={classes.root}>
            <div className={classes.leftSide}>
                <h1 className={classes.title}>{title}</h1>
            </div>
            { status.message && (
                <Group px={12} sx={{ borderRadius: 100, backgroundColor: badgeColor, height: 24 }}>
                    <Text color={badgeTextColor} size={"xs"} transform={"uppercase"} sx={{ whiteSpace: "nowrap" }}>
                        {status.message}
                    </Text>
                </Group>
            )}
        </div>
    );
}
