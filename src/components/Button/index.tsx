import { PropsWithChildren, useCallback } from "react";
import { createStyles, Group, Loader, UnstyledButton, UnstyledButtonProps } from "@mantine/core";

// Props
type ButtonProps = UnstyledButtonProps<any> & {
    variant: "filled" | "outlined";
    color: "primary" | "secondary";
    width?: number | string;
    height?: number | string;
    disabled?: boolean;
    loading?: boolean;
    onClick?: () => void;
};

// Styling
const useStyles = createStyles(() => ({
    button: {
        cursor: 'pointer',
        borderRadius: 4,
        fontSize: 14,
        lineHeight: '20px',
        fontWeight: 500,
        alignItems: 'center',

        '&': {
            transitionTimingFunction: 'ease-in-out',
            transitionDuration: '150ms',
            transitionProperty: 'background-color, color, border, box-shadow'
        }
    },
    filled: {
        backgroundColor: "#4700e5",
        boxShadow: "0px 4px 12px rgba(55, 81, 255, 0.24)",
        color: "#ffffff",

        "&:hover": {
            backgroundColor: "#1c00a6",
            boxShadow: "0px 6px 24px rgba(37, 61, 217, 0.32)",
            color: "#ffffff",
        },

        "&:active": {
            backgroundColor: "#4700e5",
            boxShadow: "0px 2px 6px rgba(37, 61, 217, 0.32)",
            color: "#ffffff",
        },

        "&:disabled": {
            backgroundColor: "#b8bde0",
            boxShadow: "none",
            color: "#ffffff",
        },
    },
    primaryOutlined: {
        border: "1px solid #4700e5",
        backgroundColor: "transparent",
        color: "#4700e5",

        "&:hover": {
            border: "1px solid #1c00a6",
            backgroundColor: "transparent",
            color: "#1c00a6",
        },

        "&:active": {
            backgroundColor: "#4700e5",
            color: "#ffffff",
        },

        "&:disabled": {
            border: "1px solid #b8bde0",
            backgroundColor: "transparent",
            color: "#b8bde0",
        },
    },
    secondaryOutlined: {
        backgroundColor: "#fcfdfe",
        border: "1px solid #dddeee",
        color: "#666892",

        "&:hover": {
            backgroundColor: "#fcfdfe",
            border: "1px solid #1c00a6",
            color: "#1c00a6",
        },

        "&:active": {
            backgroundColor: "#fcfdfe",
            border: "1px solid #dddeee",
            color: "#4700e5",
        },

        "&:disabled": {
            backgroundColor: "#fcfdfe",
            border: "1px solid #dddeee",
            color: "#b8bde0",
        },
    },
}));

// Component
export function Button<T extends React.ElementType = "button">(
    props: PropsWithChildren<ButtonProps & Omit<React.ComponentPropsWithoutRef<T>, keyof ButtonProps>>
) {
    const { children, variant, color, as = "button", width, height, onClick, disabled, loading, type, ...rest } = props;

    const { cx, classes } = useStyles();
    const loaderColor = variant === "filled" ? "#ffffff" : "#4700e5";

    const onClickHandler = useCallback(() => {
        if (disabled || loading) return;

        onClick && onClick();
    }, [disabled, loading, onClick]);

    return (
        <UnstyledButton
            align={"center"}
            style={{ width, height, justifyContent: "center" }}
            {...rest}
            type={type}
            disabled={disabled || loading}
            onClick={onClickHandler}
            component={as as any}
            className={cx(
                classes.button,
                variant === "filled" && classes.filled,
                variant === "outlined" && color === "primary" && classes.primaryOutlined,
                variant === "outlined" && color === "secondary" && classes.secondaryOutlined
            )}
        >
            <Group spacing={16} sx={{ position: "relative", justifyContent: "center", height: "100%" }}>
                {loading && (
                    <Group sx={{ position: "absolute", inset: 0, zIndex: 10, justifyContent: "center" }}>
                        <Loader color={loaderColor} size="sm" />
                    </Group>
                )}

                <Group sx={{ opacity: loading ? 0 : 1, height: "100%" }}>{children}</Group>
            </Group>
        </UnstyledButton>
    );
}
