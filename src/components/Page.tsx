import {Stack} from "@mantine/core";
import {PropsWithChildren} from "react";

export const Page = ({ children }: PropsWithChildren<{}>) => (
    <Stack sx={{ flex: 1, maxHeight: '100vh', overflow: "auto"}}>
        { children }
    </Stack>
)