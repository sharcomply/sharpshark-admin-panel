import { useEffect } from 'react';

export function useWatchEffect<T>(watch: T, effect: () => void) {
    useEffect(() => {
        if (!watch) return
        
        effect()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [watch])
}
