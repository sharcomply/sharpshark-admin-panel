/// <reference types="react-scripts" />

import { DefaultMantineColor, Tuple } from '@mantine/core';

type CustomColors = 'accent' | 'grayscale' | 'green' | 'yellow' | 'red' | DefaultMantineColor;

declare module '@mantine/core' {
  export interface MantineThemeColorsOverride {
    colors: Record<CustomColors, Tuple<string, 10>>;
  }
}
