import {MantineThemeOverride} from "@mantine/core";

export const Theme: MantineThemeOverride = {
    fontFamily: "SuisseInt`l",
    colors: {
        accent: ['#EDEEFF', '#DDE2FF', '#B8BDE0', '#4700E5', '#1C00A6'],
        grayscale: ['#FCFDFE', '#F7F8FF', '#F0F1F7', '#DDDEEE', '#DDDEEE', '#B6B4CD', '#979CBC', '#666892', '#32313E', '#0A001E'],
        green: ['#EAFAF6', '#29CCB1'],
        yellow: ['#FDFAE4', '#FFC933'],
        rose: ['#FFF2F8', '#E50C54', '#CF084A'],
    }
}
