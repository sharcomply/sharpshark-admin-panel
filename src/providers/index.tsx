import {PropsWithChildren} from "react";
import {QueryClient, QueryClientProvider} from "react-query";
import {ReactQueryDevtools} from "react-query/devtools";
import {Provider as ReduxProvider} from "react-redux";
import {MantineProvider} from "@mantine/core";

import {store} from "store";
import {Theme} from "theme";
import {NotificationsProvider} from "@mantine/notifications";

const queryClient = new QueryClient();
const isDev = process.env.NODE_ENV === "development";

export const Providers = ({children}: PropsWithChildren<{}>) => (
    <MantineProvider theme={Theme}>

            <ReduxProvider store={store}>
                <QueryClientProvider client={queryClient}>
                    <NotificationsProvider>
                    {children}
                    </NotificationsProvider>
                    {isDev && <ReactQueryDevtools initialIsOpen={false} position={"bottom-right"}/>}
                </QueryClientProvider>
            </ReduxProvider>

    </MantineProvider>
);
