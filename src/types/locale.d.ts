export type LocaleItemType = {
    _id: string;
    name: string;
    link: string;
}

export type LocaleItemResponseType = {
    item: LocaleItemType
}

export type LocaleUploadLinkType = {
    url: string;
    fields: any;
    downloadLink: string;
}

export type CreateLocaleForm = {
    name: string;
    link: string;
}

export type UpdateLocaleParams = {
    locale: LocaleItemType;
    json: any;
}