interface LoginParams {
    email: string;
    password: string;
}

interface LoginPayload {
    authToken: string;
}
