export type PurchasesItemType = {
    _id: string;
    email: string;
    walletAddress: string;
    status: UpdatePurchaseStatus;
    amount: string;
    createdAt: string;
    updatedAt: string;
    __v: number;
}

export type PurchaseStatus = 'not processed' | 'processing' | 'processed'

export type UpdatePurchaseForm = {
    status: PurchaseStatus
}