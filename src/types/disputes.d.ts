interface Disput {
  _id: string;
  alertId: string;
  demands?: string;
  appearedOn: string;
  payAmount: number;
  publicationQuestionLinks: string[]
  publicationQuestionWebarchiveLinks: string[]
  isPublicationQuestionWebarchiveFailed: boolean
  documentId: Draft;
  publicationLinks?: string[]
  publicationWebarchiveLinks?: string[]
  isPublicationWebarchiveFailed?: boolean
  createdAt: string;
  ownerClaimText?: string;
  providerClaimText?: string;
  providerClaimText?: string;

  ownerClaimEmails?: string[]
  ownerClaimType?: string;

  providerClaimEmails?: string;

  status?: string;
}

interface EditDisput {
  _id: string;
  alertId?: string;
  demands?: string;
  appearedOn?: string;
  payAmount?: number;
  publicationQuestionLinks?: string[]
  publicationQuestionWebarchiveLinks?: string[]
  isPublicationQuestionWebarchiveFailed?: boolean
  publicationLinks?: string[]
  publicationWebarchiveLinks?: string[]
  isPublicationWebarchiveFailed?: boolean
  ownerClaimText?: string;
  providerClaimText?: string;
  providerClaimText?: string;

  ownerClaimEmails?: string[]
  ownerClaimType?: string;

  providerClaimEmails?: string;

  status?: string;
  draftsScreenshots?: string[];
  publicationQuestionScreenshots?: string[]
}

interface FindEmails {
  emails: string[]
}

interface GetUploadLink {
  downloadLink: string;
  uploadLink: {
    url: string;
    fields: any;
  };
}