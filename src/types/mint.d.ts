export type MintItemType = {
    _id: string;
    email: string,
    walletAddress: string,
    tokenId: string,
    createdAt: string
    updatedAt: string,
    __v: number
}