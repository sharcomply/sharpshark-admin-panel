interface ApiResponse<T> {
    readonly success: boolean;
    readonly statusCode: number;
    readonly data: T;
}

interface ApiResponseList<T> {
    readonly success: boolean;
    readonly statusCode: number;
    readonly data: {
        items: T[];
        count: number;
    };
}
