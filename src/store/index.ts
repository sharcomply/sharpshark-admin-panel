import { configureStore } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';

// Slices
import modalSlice from 'features/Modals/store';
import authSlice from "features/Auth/store"
import localeSlice from "features/Locale/store"
import { wizzardFromReducers } from 'features/DisputForms/store';

export const store = configureStore({
    reducer: {
        modals: modalSlice.reducer,
        auth: authSlice.reducer,
        locales: localeSlice.reducer,
        wizzardFrom: wizzardFromReducers
    },
    devTools: {
        actionCreators: {
            ...modalSlice.actions,
            ...authSlice.actions,
            ...localeSlice.actions
        }
    }
});

export type AppState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<AppState> = useSelector;
