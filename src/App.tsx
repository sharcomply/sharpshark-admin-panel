import ModalContainer from "features/Modals";

import Routes from "features/Routes";

import "assets/styles/global.css";
import {Providers} from "providers";
import styled from "@emotion/styled";

export default function App() {
  return (
    <Providers>
      <Version>v1.05</Version>
      <ModalContainer/>
      <Routes/>
    </Providers>
  );
}

const Version = styled.div`
  position: absolute;
  left: 0px;
  bottom: 0px;
  background-color: rgb(0, 215, 0);
  border-top-right-radius: 8px;
  z-index: 9999;
  padding: 2px 8px;
  user-select: none;
`
