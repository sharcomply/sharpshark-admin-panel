import {fetchPurchases} from "../../../api/purchases";
import {useQuery} from "react-query";

export const usePurchases = (skip: number = 0, search?: string, sort?: any[], selectedFilters?: any) => {
    const sortOptions:any = []
    sort?.forEach((elem) => {
        const value = [elem.name, elem.value ? 1 : -1]
        sortOptions.push(value)
    })

    const filter = selectedFilters ? selectedFilters.value : ''

    const {data, isLoading, refetch} = useQuery([`PURCHASES_LIST`, {skip, search, sortOptions, filter}], () => fetchPurchases(skip * 15, search!, sortOptions, filter), {
        keepPreviousData: false,

    })

    const pages = data?.count ? Math.ceil(data.count / 15) : 0

    return {data, pages, isLoading, refetch}
}