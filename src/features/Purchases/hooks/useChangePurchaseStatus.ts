import {useCallback, useState} from "react";
import {PurchaseStatus} from "../../../types/purchases";
import {updatePurchaseRequest} from "../../../api/purchases";
import {showNotification} from "@mantine/notifications";

export const useChangePurchaseStatus = (status:PurchaseStatus, id: string) => {
    const [currentStatus, setStatus] = useState(status)
    const [loading, setLoading] = useState(false)

    const handleChange = useCallback((value:PurchaseStatus) => {
        if(value === currentStatus) return

        setLoading(true)

        const payload = {
            status: value
        }

        updatePurchaseRequest(id, payload).then(() => {
            setStatus(value)
            setLoading(false)
            showNotification({
                title: 'Success',
                message: 'Status successfully changed',
                styles: (theme) => ({
                    root: {
                        '&::before': {backgroundColor: theme.colors.green},
                    },

                }),
            })

        }).catch(() => {
            setLoading(false)
        })

    }, [currentStatus, id, setLoading, setStatus])

    return {
        handleChange,
        loading,
        currentStatus
    }
}