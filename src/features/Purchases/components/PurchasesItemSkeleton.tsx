import {createStyles, Group} from "@mantine/core";

export const PurchasesItemSkeleton = () => {

    const {classes} = useStyles()
    return (
        <tr className={classes.tr}>
            <td style={{width: '20%'}}>
                <Group px={16} align="center" sx={{height: 54}}>

                </Group>
            </td>
            <td style={{width: '30%'}}>
                <Group px={16} align="center" sx={{height: 54}}>

                </Group>
            </td>
            <td style={{width: '30%'}}>
                <Group px={16} align="center" sx={{height: 54}}>
                </Group>

            </td>
            <td style={{width: '20%'}}>
                <Group px={16} align="center" sx={{height: 54}}>
                </Group>

            </td>

        </tr>
    );
};

const useStyles = createStyles(() => ({
    tr: {
        transition: "background-color 200ms linear",
        borderTop: "1px solid #dddeee",
        '&:hover': {
            backgroundColor: "#edeeff"
        }
    },
}));
