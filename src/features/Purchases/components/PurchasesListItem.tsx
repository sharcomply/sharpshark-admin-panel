import {createStyles, Group, Select, Text} from "@mantine/core";
import dayjs from "dayjs";
import {PurchaseStatus} from "../../../types/purchases";
import {useChangePurchaseStatus} from "../hooks/useChangePurchaseStatus";


type componentProps = {
    id: string,
    email: string,
    address: string,
    status: PurchaseStatus,
    amount: string,
    createdAt: string
}

const PurchaseStatuses = [
    {value: 'not processed', label: 'Not processed'},
    {value: 'processing', label: 'Processing'},
    {value: 'processed', label: 'Processed'}
]


export const PurchasesListItem = ({id, amount, status, createdAt, email, address}: componentProps) => {
        const {handleChange, loading, currentStatus} = useChangePurchaseStatus(status, id)
        const createdDate = dayjs(createdAt).format("DD/MM/YYYY HH:mm")

        const {classes} = useStyles()

        return (
            <tr className={classes.tr}>
                <td>
                    <Group px={16} align="center" sx={{height: 54}}>
                        <Text
                            size={"sm"}
                            color={"#0A001E"}
                            weight={"500"}
                            style={{
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                                whiteSpace: "nowrap",
                            }}
                        >
                            {email ? email : 'EMAIL NOT FOUND'}
                        </Text>
                    </Group>
                </td>
                <td>
                    <Group px={16} align="center" sx={{height: 54}}>
                        <Text
                            size={"sm"}
                            color={"#0A001E"}
                            weight={"500"}
                            style={{
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                                whiteSpace: "nowrap",
                            }}
                        >
                            {address}
                        </Text>
                    </Group>
                </td>
                <td style={{width: '30%'}}>
                    <Group px={16} align="center" sx={{height: 54}}>
                        <Select disabled={loading} data={PurchaseStatuses} onChange={handleChange} value={currentStatus}/>
                    </Group>

                </td>
                <td>
                    <Group px={16} align="center" sx={{height: 54}}>
                        <Text
                            size={"sm"}
                            color={"#0A001E"}
                            weight={"500"}
                            style={{
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                                whiteSpace: "nowrap",
                            }}
                        >
                            {amount} USD
                        </Text></Group>

                </td>
                <td>
                    <Group px={16} align="center" sx={{height: 54}}>
                        <Text
                            size={"sm"}
                            color={"#0A001E"}
                            weight={"500"}
                            style={{
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                                whiteSpace: "nowrap",
                            }}
                        >
                            {createdDate}
                        </Text>
                    </Group>

                </td>

            </tr>
        );
    }
;

const useStyles = createStyles(() => ({
    tr: {
        transition: "background-color 200ms linear",
        borderTop: "1px solid #dddeee",
        '&:hover': {
            backgroundColor: "#edeeff"
        }
    },
}));
