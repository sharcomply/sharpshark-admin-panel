import {createStyles, LoadingOverlay, Pagination, Stack, Text, TextInput} from "@mantine/core";
import {TableHead} from "../../../components/TableHead";
import {Fragment, useCallback, useState} from "react";
import {usePurchases} from "../hooks/usePurchases";
import {PurchasesListItem} from "./PurchasesListItem";
import {PurchasesItemSkeleton} from "./PurchasesItemSkeleton";
import {sortsTypes} from "../../../helpers/sortConstants";

const defaultSorts = [[]]

const filters = [
    {
        name: 'status',
        items: [
            {label: "Processed", value: 'processed'},
            {label: "Not processed", value: 'not processed'},
            {label: "Processing", value: 'processing'}
        ]
    }
]

export const PurchasesList = () => {
    const {classes} = useStyles()
    const [currentPage, setCurrentPage] = useState(1)
    const [search, setSearch] = useState("")
    const [sortOptions, setSortOptions] = useState(defaultSorts)
    const [selectedFilters, setSelectedFilters] = useState()
    const {data: purchases, pages, isLoading, refetch} = usePurchases(currentPage - 1, search, sortOptions, selectedFilters)


    const handleChangeSort = useCallback(async (values: any) => {
        setSortOptions(values)
        await refetch()
    }, [setSortOptions])

    const handleSearch = useCallback(async (value: string) => {
        setSearch(value)
        setCurrentPage(1)
    }, [setSearch, setCurrentPage])

    const handleFilter = useCallback(async(filters:any) => {
        setSelectedFilters(filters)
        setCurrentPage(1)
    }, [setSelectedFilters])

    return (
        <Stack className={classes.stack}>
            <Stack align={'flex-end'} m={8}>
                <TextInput placeholder={"Search"} value={search} onChange={(value) => handleSearch(value.target.value)}/>
            </Stack>
            <table className={classes.table}>
                <TableHead items={[
                    {title: 'Email', field: 'email', value: false, sortType: sortsTypes.string, width: '20%'},
                    {title: 'Wallet Address', field: "walletAddress", sortType: sortsTypes.string, value: false, width: '30%'},
                    {title: 'Status', width: '20%'},
                    {title: 'Amount', field: 'amount', sortType: sortsTypes.number, value: false, width: '15%'},
                    {title: 'Created At', field: 'createdAt', sortType: sortsTypes.date, value: false, width: '15%'},
                ]} change={handleChangeSort} filters={filters} changeFilters={handleFilter}/>

                <tbody style={{position: 'relative'}}>
                <tr>
                    <td>
                        <LoadingOverlay visible={isLoading}/>
                    </td>
                </tr>
                {isLoading && Array.from({length: 15}).map((_, index) => (
                    <PurchasesItemSkeleton
                        key={`${index}`}
                    />
                ))}
                <Fragment>

                    {purchases?.items.map((purchase, index) => (
                        <PurchasesListItem
                            key={`${purchase._id}-${index}`}
                            email={purchase.email}
                            id={purchase._id}
                            address={purchase.walletAddress}
                            createdAt={purchase.createdAt}
                            status={purchase.status}
                            amount={purchase.amount}
                        />
                    ))}

                </Fragment>
                {!isLoading && !purchases?.count && (
                    <tr style={{borderTop: '1px solid #DDDEEE'}}>
                        <td style={{padding: 16}}>
                            <Text size={'sm'} color={'#666892'} weight={'400'}>This is a blank page... Cases will appear here</Text>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                )}
                </tbody>
            </table>
            <Stack align={'center'} style={{marginBlock: '16px'}}>
                {pages > 1 &&
                    <Pagination style={{margin: '0 auto'}} size="lg" page={currentPage} onChange={setCurrentPage} total={pages}/>
                }
            </Stack>

        </Stack>
    )
}


const useStyles = createStyles(() => ({
    stack: {
        backgroundColor: "#ffffff",
        border: "1px solid #dddeee",
        borderRadius: "8px",
        margin: "40px",
        marginTop: "65px",
    },
    table: {
        width: "100%",
        tableLayout: "fixed",
        borderCollapse: "collapse",
        overflowWrap: "anywhere"
    }
}));

