import {ActionIcon, createStyles, Stack, Text} from "@mantine/core"
import {TableHead} from "components/TableHead";
import {Fragment, useCallback} from "react";
import {LocaleListItem} from "./LocaleListItem"
import {Icon} from "../../../components";
import {useLocales} from "../hooks/useLocales";
import {useModal} from "../../Modals/hooks/useModal";
import {ModalContent, ModalType} from "../../Modals/interfaces";


export const LocaleList = () => {
    const {data: locales} = useLocales()
    const {actions} = useModal(ModalType.LOCALE)

    const handleCreateLocale = useCallback(() => {
        actions.open({type: ModalType.LOCALE, content: ModalContent.LOCALE_CREATE})
    }, [actions])

    const {classes} = useStyles()

    return (
        <Stack className={classes.stack}>
            <table className={classes.table}>
                <TableHead items={[
                    {title: 'language', width: '50%'},
                ]}/>

                <tbody>
                <Fragment>
                    {!!locales?.items.length && locales.items.map((locale) => (
                        <LocaleListItem item={locale} key={locale._id}/>
                    ))}
                </Fragment>

                {!locales?.items.length && <tr style={{borderTop: '1px solid #DDDEEE'}}>
                    <td style={{padding: 16}}>
                        <Text size={'sm'} color={'#666892'} weight={'400'}>This is a blank page... Cases will appear here</Text>
                    </td>
                </tr>
                }
                </tbody>
            </table>

            <div className={classes.buttonWrapper}>
                <ActionIcon onClick={handleCreateLocale} className={classes.buttonStyles} size={64} color={"blue"} radius={50} variant="filled">
                    <Icon size={36} icon="akar-icons:plus" />
                </ActionIcon>
            </div>
        </Stack>
    )
}

const useStyles = createStyles(() => ({
    stack: {
        backgroundColor: "#ffffff",
        border: "1px solid #dddeee",
        borderRadius: "8px",
        margin: "40px",
        marginTop: "65px",
    },
    table: {
        width: "100%",
        tableLayout: "fixed",
        borderCollapse: "collapse",
        overflowWrap: "anywhere",
    },
    buttonWrapper: {
        position: 'fixed',
        bottom: '40px',
        right: '40px',
        zIndex: 100
    },
    buttonStyles: {
        fontSize: '36px',
    }
}))

