import {ActionIcon, createStyles, Group, Text} from "@mantine/core";
import {Icon} from "components";
import {Link} from "react-router-dom";
import {LocaleItemType} from "../../../types/locale";
import {useModal} from "../../Modals/hooks/useModal";
import {ModalContent, ModalType} from "../../Modals/interfaces";
import {useCallback} from "react";
import {useLocaleStore} from "../hooks/useLocaleStore";

type componentProps = {
    item: LocaleItemType
}

const useStyles = createStyles(() => ({
    tr: {
        transition: "background-color 200ms linear",
        borderTop: "1px solid #dddeee",

        "&:hover": {
            backgroundColor: "#edeeff",
        }
    }
}))

export const LocaleListItem = ({item}: componentProps) => {
    const {classes} = useStyles()

    const {actions: storeActions} = useLocaleStore()
    const {actions: modalActions} = useModal(ModalType.LOCALE)

    const handleDelete = useCallback(() => {
        storeActions.setRemovePayload({id: item._id, name: item.name});
        modalActions.open({type: ModalType.LOCALE, content: ModalContent.LOCALE_DELETE});
    }, [modalActions, storeActions, item])

    return (
        <tr className={classes.tr}>
            <td style={{width: "100%"}}>
                <Link to={`/locales/${item.name}`}>
                    <Group px={16} align="center" sx={{height: 54}}>
                        <Text
                            size={"sm"}
                            color={"#0A001E"}
                            weight={"500"}
                            style={{
                                overflow: "hidden",
                                textOverflow: "ellipsis",
                                whiteSpace: "nowrap",
                            }}
                        >
                            {item.name}
                        </Text>
                    </Group>
                </Link>
            </td>

            <td>
                <Group px={16} position="right" align="center" sx={{height: 54}}>
                    <ActionIcon onClick={handleDelete} color="red" size="xl">
                        <Icon color="red" icon="fluent:delete-12-filled"/>
                    </ActionIcon>
                </Group>
            </td>

        </tr>
    );
};
