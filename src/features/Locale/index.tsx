import { createStyles, Group, Stack, Text, TextInput } from "@mantine/core";
import PageTitle from "components/PageTitle";
import { useCallback } from "react";
import { useForm } from "react-hook-form";
import { Button } from "../../components/Button";
import { useLocale } from "./hooks/useLocale";
import { useBaseLocale } from "./hooks/useBaseLocale";
import { useLocaleMutation } from "./hooks/useLocaleMutation";
// import { Collapse } from "../../components/Collapse";

type ComponentProps = {
    localeName?: string;
};

const useStyles = createStyles(() => ({
    stack: {
        backgroundColor: "#ffffff",
        border: "1px solid #dddeee",
        borderRadius: "8px",
        position: "relative",
        gap: 0,
    },
    keyTitle: {
        display: "flex",
        alignItems: "center",
        backgroundColor: "#f1f3f5",
        color: "#909296",
        opacity: 0.6,
        cursor: "not-allowed",
        minHeight: 36,
        paddingLeft: 12,
        paddingRight: 12,
        borderRadius: 4,
        border: "1px solid #ced4da",
    },
}));

export const LocalView = ({ localeName }: ComponentProps) => {
    const { classes } = useStyles();

    const { handleUpdate, updateLoading } = useLocaleMutation();
    const {
        json,
        // localeFlattenJson,
        locale,
    } = useLocale(localeName);
    const { json: jsonBase, localeFlattenJson: localeFlattenBaseJson } = useBaseLocale();

    console.log(jsonBase);

    const form = useForm({
        defaultValues: json,
    });

    const handleSubmit = useCallback(
        async (values: any) => {
            await handleUpdate({ json: values, locale: locale! });
        },
        [locale, handleUpdate]
    );

    // const renderFields = (title: string) => {
    //     console.log(title, typeof localeFlattenJson[title]);
    //     if (typeof localeFlattenJson[title] === "object") {
    //         return Object.keys(localeFlattenJson[title]).map((key: string) => (
    //             <Group py={4} key={key}>
    //                 <Text className={classes.keyTitle} sx={{ width: "45%" }}>
    //                     {key}
    //                 </Text>
    //                 <TextInput style={{ width: "45%" }} {...form.register(`${title}.${key}`)} />
    //             </Group>
    //         ));
    //     }

    //     if (typeof localeFlattenJson[title] === "string") {
    //         return (
    //             <Group py={4}>
    //                 <Text className={classes.keyTitle} sx={{ width: "45%" }}>
    //                     {title}
    //                 </Text>
    //                 <TextInput style={{ width: "45%" }} {...form.register(`${title}`)} />
    //             </Group>
    //         );
    //     }

    //     return null;
    // };

    return (
        <form onSubmit={form.handleSubmit(handleSubmit)}>
            <Stack style={{ overflow: "auto", flex: 1 }}>
                <Stack pt={40} pb={40} px={20} sx={{ overflow: "hidden", flex: 1 }}>
                    <Group position={"apart"} align={"center"}>
                        <PageTitle title={`Locale: ${locale!.name}`} />
                        <Button loading={updateLoading} width={150} height={42} variant={"filled"} color={"primary"} type="submit">
                            Save
                        </Button>
                    </Group>

                    <Stack px={20} py={20} className={classes.stack}>
                        {Object.keys(localeFlattenBaseJson).map((title: string) => (
                            <Stack spacing={0} key={title}>
                                {Object.keys(localeFlattenBaseJson[title]).map((key: string) => {
                                    if (!form.getValues(`${title}.${key}`)) {
                                        form.setValue(`${title}.${key}`, localeFlattenBaseJson[title][key]);
                                    }
                                  return  <Group spacing={8} py={4} key={key}>
                                        <Text className={classes.keyTitle} sx={{ width: "45%" }}>
                                            {`${title}.${key}` || title}
                                        </Text>
                                        <TextInput style={{ width: "45%" }} {...form.register(`${title}.${key}`)} />
                                    </Group>
})}
                            </Stack>
                        ))}
                    </Stack>
                </Stack>
            </Stack>
        </form>
    );
};


