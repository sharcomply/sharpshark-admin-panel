import {createSlice, PayloadAction} from "@reduxjs/toolkit";


export interface ILocaleStore {
    remove: {
        id?: string;
        name?: string;
    }
}

const initialState: ILocaleStore = {
    remove: {
        id: undefined,
        name: undefined
    }
};

const slice = createSlice({
    name: "locales",
    initialState,
    reducers: {
        setRemovePayload: (state, action: PayloadAction<ILocaleStore['remove']>) => {
            state.remove = action.payload
        }
    },
});

export default slice
