import { useMemo } from "react";
import { useQuery } from "react-query";
import { fetchFile } from "../../../api/locales";

export const useBaseLocale = () => {
    const baseLocaleUrl = "https://d1k1dlls6f6nna.cloudfront.net/locales/base.json";

    const { data: json, isLoading: jsonLoading } = useQuery(["BASE_LOCALE"], () => fetchFile(baseLocaleUrl), {
        retry: false,
        keepPreviousData: true,
    });

    const localeFlattenJson = useMemo(() => {
        if (!json) return undefined;

        const flattenJson: any = {};

        Object.keys(json).forEach((key: string) => {
            const flatObject = flatten(json[key]);

            flattenJson[key] = flatObject;
        });

        return flattenJson;
    }, [json]);

    return {
        isLoading: jsonLoading,
        json,
        localeFlattenJson,
    };
};

const flatten = (object: any) => {
    const path: any = {};

    const iter = (obj: any, arr: any) => {
        if (obj && typeof obj === "object") {
            Object.keys(obj).forEach((key) => iter(obj[key], arr.concat(key)));
            return;
        }
        path[arr.join(".")] = obj;
    };

    iter(object, []);
    return path;
};
