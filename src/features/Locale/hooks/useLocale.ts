import {useLocales} from "./useLocales";
import {useMemo} from "react";
import {fetchFile} from "../../../api/locales";
import {useQuery} from "react-query";

export const useLocale = (name?: string) => {
    const { data, isLoading } = useLocales();

    const locale = useMemo(() => {
        return data?.items.find((item) => item.name.toLowerCase() === name?.toLowerCase()) || undefined
    }, [data, name])

    const { data: json , isLoading: jsonLoading } = useQuery(["LOCALE", locale?.name], () => fetchFile(locale?.link), {
        enabled: !!locale,
        retry: false,
        keepPreviousData: true
    })


    const localeFlattenJson = useMemo(() => {
        if (!json) return undefined

        const flattenJson: any = {}

        Object.keys(json).forEach((key: string) => {
            const flatObject = flatten(json[key]);

            flattenJson[key] = flatObject
        })

        return flattenJson
    }, [json])

    return {
        isLoading: isLoading || jsonLoading,
        locale,
        json,
        localeFlattenJson
    }
}


function flatten(object: any) {
    const path: any = {};

    const iter = (obj: any, arr: any) => {
        if (obj && typeof obj === "object") {
            Object.keys(obj).forEach((key) => iter(obj[key], arr.concat(key)));
            return;
        }
        path[arr.join(".")] = obj;
    };

    iter(object, []);
    return path;
};