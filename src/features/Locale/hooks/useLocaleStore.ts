import {useAppDispatch, useAppSelector} from "store";
import {useCallback} from "react";
import localeSlice, {ILocaleStore} from "../store";

export const useLocaleStore = () => {
    const state = useAppSelector((state) => state.locales);
    const dispatch = useAppDispatch();

    // Actions

    const setRemovePayload = useCallback((payload: ILocaleStore['remove']) => {
        const action = localeSlice.actions.setRemovePayload(payload)
        dispatch(action)
    }, [dispatch])


    return {
        state,
        actions: {
            setRemovePayload
        }
    };
};