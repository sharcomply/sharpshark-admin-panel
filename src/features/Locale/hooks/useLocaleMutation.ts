import {useCallback, useState} from "react";
import {UpdateLocaleParams} from "types/locale";
import {useMutation, useQueryClient} from "react-query";
import {backend} from "api";
import {useNotifications} from "@mantine/notifications";
import {useNavigate} from "react-router-dom";

export const useLocaleMutation = () => {
    const queryClient = useQueryClient();
    const { showNotification } = useNotifications()
    const navigate = useNavigate()

    const [createLoading, setCreateLoading] = useState(false);
    const [updateLoading, setUpdateLoading] = useState(false);
    const [removeLoading, setRemoveLoading] = useState(false);

    const createMutation = useMutation((name: string) => backend.locales.createLocale(name));
    const updateMutation = useMutation((params: UpdateLocaleParams) => backend.locales.updateLocale(params));
    const deleteMutation = useMutation((localeId: string) => backend.locales.deleteLocationRequest(localeId))

    // handlers
    const handleCreate = useCallback(async (name: string) => {
        setCreateLoading(true)

        try {
            await createMutation.mutateAsync(name);
            await queryClient.invalidateQueries(['LOCALES_LIST']);
            setCreateLoading(false);
            navigate(`/locales/${name}`)
            showNotification({ title: 'Success', message: `Locale: ${name} was successfully created.`, color: 'teal' })
        } catch(e) {
            setCreateLoading(false);
            showNotification({title: 'Error', message: `Something went wrong, try again later.`, color: 'red' })
        }
    }, [setCreateLoading]);

    const handleUpdate = useCallback(async (params: UpdateLocaleParams) => {
        setUpdateLoading(true);

        try {
            await updateMutation.mutateAsync(params);
            await queryClient.invalidateQueries(['LOCALE', params.locale.name]);
            setUpdateLoading(false);
            showNotification({ title: 'Success', message: `Locale: ${params.locale.name} was successfully updated.`, color: 'teal' })
        } catch(e) {
            setUpdateLoading(false);
            showNotification({title: 'Error', message: `Something went wrong, try again later.`, color: 'red' })
        }
    }, [setUpdateLoading]);

    const handleRemove = useCallback(async (id: string, name?: string) => {
        setRemoveLoading(true);

        try {
            await deleteMutation.mutateAsync(id);
            await queryClient.invalidateQueries(['LOCALES_LIST']);
            setRemoveLoading(false);
            showNotification({ title: 'Success', message: `Locale${`: ${name}` || ':'} was successfully removed.`, color: 'teal' })
        } catch(e) {
            setRemoveLoading(false);
            showNotification({title: 'Error', message: `Something went wrong, try again later.`, color: 'red' })
        }
    }, [setRemoveLoading]);

    return {
        handleCreate,
        createLoading: createLoading || createMutation.isLoading,
        handleUpdate,
        updateLoading: updateLoading || updateMutation.isLoading,
        removeLoading: removeLoading || deleteMutation.isLoading,
        handleRemove
    }
}