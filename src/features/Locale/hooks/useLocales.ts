import {useQuery} from "react-query";
import {fetchLocales} from "../../../api/locales";

export const useLocales = () => {
    const {data, isLoading} = useQuery(['LOCALES_LIST'], () => fetchLocales())

    return {
        data,
        isLoading
    }
}