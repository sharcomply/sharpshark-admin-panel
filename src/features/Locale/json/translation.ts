export const StandardLocale: any = {
    "sidebar": {
        "drafts_btn": "Drafts",
        "protected_btn": "Protected",
        "copyright-protection_text": "Copyright Protection",
        "alerts_btn": "Alerts",
        "disputes_btn": "Disputes",
        "new-document_btn": "Create a new document",
        "how-it-works_link": "How it works",
        "back_btn_text": "Back"
    },
    "buttons": {
        "protect_btn": "Protect",
        "change_email_btn": "Change Email",
        "add_email_btn": "Add Email",
        "change-email_btn": "Continue",
        "password_change_btn": "Change password",
        "log-out_btn_text": "Log Out",
        "clean_btn": "Clean",
        "login_btn": "Login",
        "back_btn": "Back",
        "save_close_btn": "Save & close",
        "sign_up_sign_in_btn": "Sign up or Sign in",
        "continue_demo_btn": "Continue to demo",
        "unlocked_btn": "Unlocked mode",
        "locked_btn": "Locked mode",
        "manager_btn": "Connect... (coming soon)",
        "continue_btn": "Continue",
        "skip_btn": "Skip",
        "delete_forever_btn": "Delete forever",
        "add_btn": "ADD",
        "remove_btn": "Remove",
        "new_btn": "New",
        "mint_btn": "Mint",
        "got_it_btn": "Got it",
        "nice_btn": "Nice",
        "all_clear_btn": "All clear!",
        "restore_btn": "Restore",
        "add_name_btn": "Add name (coming soon)",
        "change_name_btn": "Change name (coming soon)",
        "reset_password_btn": "Reset password",
        "sign_up_btn": "Sign up",
        "google_sign_up_btn": "Use Google ID instead",
        "save_btn": "Save",
        "cancel_btn": "Cancel",
        "metamask_pay_btn": "Pay with Metamask",
        "fiat_pay_btn": "Pay with Fiat",
        "reload_page_btn": "Reload page",
        "skip_for_now_btn": "Skip for now",
        "metamask_login_btn": "Login via MetaMask",
        "sign_in_btn": "Sign in",
        "confirm_btn": "Confirm",
        "upgrade_btn": "Upgrade",
        "view_document_btn": "View Document",
        "add_new_version_btn": "Add a new version",
        "view_certificate_btn": "View certificate",
        "monitoring_again_btn": "Monitoring again",
        "stop_monitoring_btn": "Stop Monitoring",
        "tracking_btn": "Tracking...",
        "track_now_btn": "Track it now",
        "update_this_version": "Update this version",
        "connect_another_wallet": "Connect another wallet...",
        "switch_too_btn": "Switch to",
        "active_btn": "Active",
        "disconnect_btn": "Disconnect",
        "buy_licence_btn": "Buy licence",
        "change_network_btn": "Change network",
        "replenish_balance_btn": "Replenish the balance",
        "connect_wallet_btn": "Connect wallet",
        "pass_kyc_btn": "Update my legal & language data"
    },
    "statuses": {
        "saved": "Saved",
        "uploading": "Uploading...",
        "uploaded": "Uploaded",
        "upload_failed": "Failed to upload",
        "protecting": "On it",
        "protected": "All set!",
        "protect_failed": "Oops!",
        "locked": "Locked",
        "email_verification": "Email verification...",
        "failed": "failed",
        "tracking": "Tracking..."
    },
    "overlay": {
        "messages": {
            "waiting_payment": "Waiting payment confirmation for anti-plagiarism service fees",
            "check_metamask": "Check your metamask.",
            "check_stripe": "Check your payment on the stripe.",
            "payment_received": "Payment received, we are checking your work for plagiarism.",
            "wait_result": "Please, wait for results",
            "work_ready_for_upload": "The work is ready for being uploaded into decentralized network {{network}} network.",
            "we_prepared_commit": "Check your metamask, we have prepared a transaction commit, the fee for blockchain calculations to anchor the data will be taken, and than our contract will save your data into decentralized journal of {{network}} network",
            "data_anchored": "Your data has been anchored to decentralized journal:",
            "work_token_id": "tokenId of your work '{{tokenId}}', contract address '{{to}}'",
            "already_published": "Seems it was already published on the Internet:",
            "third_party_quotes": "Or are there any third party quotes in your text?",
            "see_where_link": "see where",
            "work_checked": "Your work has been checked, status: \"successful\"",
            "preparing_work": "We are preparing your work for saving to decentralized network of IPFS",
            "metadata_uploaded": "Creating document data for your document and saving it to decentralized network of IPFS",
            "scan_failed": "The content is already on the web",
            "dont_leave": "Otherwise it will lead to session drop and you will need to pay for check again. You can check transaction status via your metamask.",
            "waiting_mint": "We are waiting when blockchain will send confirmation event for anchoring prepared data for your work to decentralized journal of the {{network}} blockchain network"
        },
        "titles": {
            "dont_leave": "Please, do not leave the page until transaction is completed."
        }
    },
    "errors": {
        "titles": {
            "choose_licence_canceled": "Choice of license canceled",
            "protection_canceled": "Protection canceled",
            "auth": "Authorization error",
            "validation": "Validation Error",
            "mint_canceled": "Mint canceled",
            "smth_wrong": "Something went wrong :-(",
            "execution": "Execution reverted",
            "draft_empty": "This draft is empty",
            "scan_failed": "The content is already on the web",
            "didnt_protect_prev_version": "Looks like you didn't protect previous version",
            "monitoring_failed": "Start Monitoring failed!",
            "network_not_supported": "This network isn't supported"
        },
        "messages": {
            "choose_licence_canceled": "Payment for committing data in a distributed log has been cancelled.",
            "reconnect_to_the_network": "Execution reverted: Caller is not the minter. Reconnect to the network where the draft was protected",
            "protection_canceled": "Payment for operation not received. Protection process canceled",
            "no_metadata": "Can't find metadata. Please try again later",
            "auth": "Your are not authorized",
            "mint_canceled": "Payment for committing data in a distributed log has been cancelled. Draft protection cancelled",
            "smth_wrong": "Seems it was already published on the Internet: see where. \n Or are there any third party quotes in your text? Hide them via",
            "execution": "Token already minted",
            "draft_empty": "There is a chance that networking exception happened during file uploading. Please, try again if this draft wasn't left empty intentionally.",
            "we_failed_to_check_file": "Ah, we failed to send check your file. Please, try again or contact us!",
            "scan_failed": "Seems it was already published on the Internet: {{link}}. <br/> Or are there any third party quotes in your text?",
            "monitoring_failed": "Payment for operation not received. Monitoring no started",
            "small_length": "Draft \"text\" must be min 50 characters"
        },
        "email_verification_failed": "Email verification failed"
    },
    "warn": {
        "titles": {
            "do_not_exit_protection": "Please don’t close the window"
        },
        "messages": {
            "do_not_exit_protection": "If you start over, you’ll have to pay again. You can also check the progress in your Metamask"
        }
    },
    "warnings": {
        "rotate_horizontally": "Please rotate you tablet horizontally",
        "dont_support_mobile": "Aww, sorry, we don’t support mobile yet.",
        "enjoy_desktop": "Please enjoy Shark on your desktop or table for now"
    },
    "responses": {
        "email_verification_success": "Email verification success, now you can close this page"
    },
    "textfields": {
        "title": "Title",
        "metadataTitle": "Metadata Title",
        "coAuthors": "coAuthors",
        "coAuthor": "coAuthor",
        "participants": "Participants",
        "participant": "Participant",
        "authors": "Authors",
        "we_sent_email_code": "We sent you a code via email. Didn't get? We can resend it in {{seconds}} sec",
        "placeholders": {
            "your_password": "Your password",
            "repeat_password": "Repeat password",
            "password_validation": "Password (min. 8 characters)",
            "days_count": "Day count",
            "code": "code",
            "email_address": "Email address",
            "name": "name"
        },
        "descriptions": {
            "password_field": "8 latin characters with numbers & symbols",
            "how_many_days": "Specify how many days the monitoring will be"
        },
        "lables": {
            "create_repeat_password": "CREATE & REPEAT PASSWORD",
            "fullname": "Full name",
            "personal_email": "Personal Email",
            "email": "Email",
            "your_email": "Your email",
            "password": "Password",
            "days": "Days",
            "work_email": "Work email",
            "current_work_version": "Current work version",
            "full_title": "Full title",
            "name_or_pen_name": "NAME OR PEN NAME",
            "name_optional": "NAME (OPTIONAL)",
            "type": "type"
        }
    },
    "tooltips": {
        "drafts_arent_published": "Draft’s aren’t published or tracked for copyright violations",
        "drafts_arent_protected": "Drafts aren’t protected or published",
        "save_close": "Save and close and work on it later",
        "protected": "Protected works are published in IPFS and timestamped in the blockchain. Learn more",
        "posible_copyright_violation": "Possible copyright violation",
        "your_open_disputes": "Your open disputes",
        "change_network": "To change network.<br/> Open metamask. We support Eth, BSC and Polygon",
        "KYC_Email": "Required to access your account if you lose access to your wallet.",
        "KYC_Country": "Required for dispute resolution process, depending on the laws of your country.",
        "KYC_Language": "Required for for your official certificate generation and automated generation of claims for dispute resolution process."
    },
    "modals": {
        "password": {
            "title": "Change password",
            "sub_title_1": "Old password",
            "input_label_1": "Forgot password? Get a reminder",
            "input_placeholder_1": "Old password",
            "input_label_2": "8 latin characters with numbers & symbols",
            "input_placeholder_2": "New password",
            "reset_password_text": "You can reset password using your associated email",
            "dont_have_account": "Don’t have an account?"
        },
        "sign_up": {
            "title": "Sign up to <br/> SharpShark",
            "lets_meet": "First, let’s meet",
            "you_are": "YOU ARE...",
            "head_of_team": "Head of team",
            "individual": "Individual",
            "have_an_account": "Already have an account?"
        },
        "email": {
            "title": "Add an email",
            "sub_title": "New Email",
            "input_label": "Monotonically develop visionary benefits vis-a-vis granular data. Completely transform bleeding-edge",
            "input_placeholder": "New email",
            "set_email": "Set an email?",
            "set_email_description": "You can add you email to get notifications about alerts and display a ’human’ readable name next to your digital signature",
            "we_fetch_associated": "We’ll also fetch the associated name there"
        },
        "publications": {
            "similar_content_title": "Websites with similar content",
            "smimilar_content_text": "The content you want to use appears to have been published on these websites. You can quote it, but you can’t protect it"
        },
        "invite_member": {
            "title": "Invite more members",
            "description": "Monotonectally develop visionary benefits vis-a-vis granular data. Completely transform bleeding-edge"
        },
        "login": {
            "use_email_or_google": "Use your associated Email or a Google account",
            "password_reminder": "Get a reminder",
            "forgot_password": "Forgot your password?",
            "sign_in": "Sign in to<br/>SharpShark",
            "use_metamask_text": "Use your a MetaMask account"
        },
        "paymeyment": {
            "title": "Upgrade draft version"
        },
        "purchase_success": "The purchase for the {{tokenId}} token was successful",
        "preferences": {
            "title": "Preferences",
            "how_do_i_set": "How do I set it up?",
            "every_persson_can_find_content": "Every person can find your content in IPFS",
            "mode": "Mode",
            "license_title": "License",
            "license_description": "Choose between commercial or noncommercial licence",
            "show_name_certificate": "Show my name in the Certificate (optional)",
            "confirm_email": "Confirm email to add name",
            "have_read_terms": "I’m okay with the content going into public databases and <br /> can’t be deleted, <u>I have read about it (coming soon)</u>",
            "no_draft_id": "Something went wrong. Can`t find draft id. <br></br> Please reload page and try again.",
            "password_protected": "Password-protected (for sensitive data)",
            "fee_info": "The final price of certificate will be indicated with a fee of {{price}}$",
            "dont_close": "Please do not close this modal window. This is necessary so that you can make sure that the license is set correctly."
        },
        "monitoring": {
            "title": "Start Tracking?",
            "descriptions": "Every day we will check your work for anti-plagiarism and send you an alert about it."
        },
        "usingYourContent": {
            "title": "Using your content",
            "text": "Insert a button or a link under your publication to show others how they can use it. See examples"
        },
        "info": {
            "basic_alert_title": "Basic Alert",
            "basic_alert_text": "Monotonically develop visionary benefits vis-a-vis granular data. Completely transform bleeding-edge",
            "collecting_evidence_alert_title": "Let’s collect evidence and get some justice",
            "collecting_evidence_alert_text": "The key to success is solid evidence. Follow our steps to structure your evidence properly",
            "generating_claim_alert_title": "Let’s create a claim",
            "generating_claim_alert_text": "Based on your evidence, this is the claim. Please check and correct it, if needed",
            "sending_alert_title": "Final step — finding the contacts",
            "sending_alert_text": "This is the hardest part but we will guide you. Find out how to reach the violator and contact them. In 24 hours we will suggest to move to the next stage or close the case, if resolved. What are the stages?",
            "all_draft_alert_title": "Drafts are saved only locally",
            "all_draft_alert_text": "Monotonically develop visionary benefits vis-a-vis granular data. Completely transform bleeding-edg"
        },
        "KYC": {
            "title": "Provide data for account protection, data resolution, and claim generation",
            "button": "Save"
        }
    },
    "header": {
        "back_btn": "Back",
        "coming_soon": "(Coming Soon)"
    },
    "drafts": {
        "deleted_draft_title": "Deleted Draft",
        "deleted_text": " Deleted on {{date}} ({{daysAgo}}). Deleted drafts are kept for 30 day, then get deleted forever",
        "tabs": {
            "working_on": "Working on",
            "deleted": "Deleted",
            "content": "Content",
            "about_usage_rights": "About & Usage Rights",
            "copyright_alerts": "Copyright Alerts",
            "disputes": "Disputes"
        },
        "new-draft": "New draft",
        "new_draft_version": "New Draft Version",
        "all-draft": "All draft",
        "edit-draft": "Edit draft",
        "title_placeholder": "Happy to see you!",
        "text_palaceholder": "It’s your workspace. Type or paste Cmd+V your content here.",
        "file-upload-text": {
            "text_1": "Or",
            "text_2": "upload a file",
            "text_3": "— we accept txt for now. More to come!"
        },
        "file-select-text": {
            "text_1": "Selected file:",
            "text_2": "[remove]"
        }
    },
    "dispute": {
        "title": "Dispute resolution",
        "tabs": {
            "ended": "Ended",
            "alerts_in_dispute": "Alerts In Disputas"
        }
    },
    "alerts": {
        "tabs": {
            "new": "New",
            "not-actual": "Not actual"
        },
        "copyright_alerts": "Copyright alerts",
        "possible_copyright_violations": "Possible copyright violations",
        "all_clear_for_now": "All clear for now",
        "know_any_potential_copyright_violations": "We’ll let you know about any potential copyright violations so you can decide what to do with them",
        "new_allert": "You have new alerts"
    },
    "protected_drafts": {
        "tabs": {
            "locked": "locked",
            "unlocked": "UNLOKED"
        },
        "usage_rights": {
            "mode": "Mode",
            "licence": "Licence",
            "price": "Price",
            "draft_created": "Draft created",
            "document_protected": "Document protected",
            "document_updated": "Document updated",
            "fee_info": "with a {{price}}$ fee"
        },
        "protected-document": "Protected documents",
        "protected_on": "Protected on {{date}}",
        "updated": "updated {{date}}",
        "protect-draft-status": {
            "text_1": "Checking your draft...",
            "text_2": "Checked. Can’t protect this draft"
        }
    },
    "table": {
        "header-title": "Title",
        "header-updated": "Updated",
        "header-protected": "Protected",
        "empty-table": "This is a blank page... Cases will appear here",
        "are_these_cases_violations": "Are these cases violations? If yes, we can call for the website owner to remove/pay for it",
        "row-options": {
            "view": "View & edit",
            "protect": "Protect",
            "delete": "Delete"
        },
        "titles": {
            "deleted": "deleted",
            "your_title": "your document title",
            "appeared_on": "appeared on",
            "discovered": "discovered",
            "not_tracking": "NOT TRACKING, since",
            "wallet_address": "wallet address",
            "connected_on": "connected on",
            "last_active": "last active",
            "title": "title",
            "table_ended": "TABLE.ENDED",
            "claim_sent": "CLAIM SENT",
            "warning_sent": "WARNING SENT",
            "url": "url",
            "similarity": "similarity"
        },
        "row-by-date": {
            "today": "TODAY",
            "this-week": "THIS WEEK",
            "last-month": "LAST MONTH",
            "earlier-this-month": "EARLIER THIS MONTH",
            "earlier": "EARLIER"
        }
    },
    "month": {
        "jan": "Jan",
        "feb": "Feb",
        "mar": "Mar",
        "apr": "Apr",
        "may": "May",
        "jun": "Jun",
        "jul": "Jul",
        "aug": "Aug",
        "sep": "Sep",
        "oct": "Oct",
        "nov": "Nov",
        "dec": "Dec"
    },
    "settings": {
        "title": "Settings",
        "tabs": {
            "wallets": "wallets",
            "info": "info",
            "pricing": "pricing"
        },
        "titles": {
            "add_name": "Add name",
            "change_name": "Change_name"
        },
        "account": {
            "info_title": "Log in Info",
            "info_lable": "Email",
            "password_title": "Password",
            "language_title": "Language",
            "language_description": "This language will be used for your official certificate as well. So it should match your jurisdiction. You can change it anytime",
            "language_list": {
                "language_1": "English",
                "language_2": "Spanish"
            }
        },
        "info": {
            "title": "Settings",
            "description": "This language will be used for your official certificate as well. It should match your jurisdiction. You can change it anytime",
            "language_title": "Language",
            "language_list": {
                "language_1": "English",
                "language_2": "Spanish"
            },
            "email_title": "Email",
            "manager_title": "Manager",
            "manager_description": "If you are invited by your manager or editor-in-chief, connect to their account via their blockchain address"
        },
        "pricing": {
            "title": "Pricing",
            "description": "This language will be used for your official certificate as well. So it should match <br /> your jurisdiction. You can change it anytime"
        }
    },
    "common": {
        "copy_link": "Copy this link",
        "link_copied": "Link copied!",
        "coming_soon": "(Coming Soon)",
        "author": "Author",
        "or": "or",
        "read_faq": "Read more in FAQ",
        "price": "price",
        "something": "Something",
        "test_network_only": "(test network only)"
    },
    "certificate": {
        "usage_rights": "Usage rights:",
        "content_immutable_form": "Content in an immutable form",
        "free_rights": "Free for commercial and non-commercial use, but needs attribution: a link to the original publication or to this certificate, name, and author’s name",
        "copyrighted_rights": "Needs licence purchase and attribution: a link to the original publication or to this certificate, name, and author’s name. You need a licence for every usage",
        "purchase_success": "The purchase for the {{tokenId}} token was successful, the purchase receipt was sent to the mail",
        "commercial_distribution_rights": "If you want to reuse this content, purchase it and attribute it with a link to the original publication, its name, author’s name, and year. If you want to quote or rewrite a small portion of it, you can do it freely with the same attribution, provided it is ’fair use’ (you’re not reusing the ’heart’ of the work)",
        "reserved_no_distribution_rights": "You cannot reuse this content unless you get permission from the author. If you want to quote or rewrite a small part, you can do it freely, provided it is ’fair use’ (you’re not reusing the ’heart’ of the work) and you attribute it with a link to the original publication, its name, author’s name, and year",
        "free_to_use_rights": "Free for commercial and non-commercial use, but needs attribution: a link to the original publication or to this certificate, name, and author’s name",
        "no_distribution_rights": "Can’t be either quoted or distributed",
        "copyrights": "© 2019-2022 <br /> SHA-256 Intellectual Property SpA",
        "attribution_neded": "attribution needed",
        "fair_use_attribution_needed": "Only fair use, attribution needed",
        "check_it_out": "Check it out",
        "first_published": "First published (coming soon)",
        "coppy_attribution": "Copy attribution (soon)",
        "check_ipsf": "Check the proof in the IPFS",
        "faq_text": "Frequent questions",
        "locked_status": "Locked",
        "check_full_history": "Check full history in blockchain",
        "locked_content": {
            "title": "This content is password-protected",
            "content": "The author decided to hide the content of the document and protected it as locked. You can check whether it was later republished unlocked by checking its<|>versions<|>. If so, you’ll see a password to access this file there. Then, you can download it by clicking on the IPFS QR-code."
        },
        "content_existed_declared_time": "This certificate allows you to check if this content existed at the declared time.<|>Read more",
        "quote_content": "If you want to quote this content, add a link to the original publication or<|>a link to this certificate<|>, please.",
        "title": "Title",
        "author": "Author",
        "co_authors": "Co-authors",
        "authors_rights": "All rights belong to the author(s) and/or their organization(s)",
        "quote_author_content": "You can quote this content with reference to the author but you can’t redistribute it in any way",
        "publish_info": {
            "publish": "First published {{dateTime}}",
            "publish_updated": "Updated {{dateTime}}, v.{{version}}",
            "desktop": {
                "update_info": "This content was updated. Click to check other versions",
                "check_blockchain_link": "Check full history in blockchain",
                "immutable_form": "Content in immutable form",
                "ipfs_proof": "Check the proof in IPFS",
                "ipfs_description": "InterPlanetary File System"
            },
            "mobile": {
                "full_history_title": "Full history",
                "full_history_value": "Symbol blockchain",
                "content_title": "Content",
                "content_value": "IPFS (immutable)"
            }
        },
        "side": {
            "copyright": "© 2019-2020<|>SHA-256 Intellectual Property SpA",
            "try_app": "Want to try SharpShark?<|>Check it out",
            "try_app_mobile": "Try SharpShark"
        },
        "faq": {
            "title": "Common questions",
            "questions": [
                "What is this? What should I do with this thing?",
                "The right of authorship emerges the moment the artwork is created — and cannot be withdrawn (not like intellectual property.) In order to prove the authorship, the author needs to prove the moment of creation of the artwork and its form. SharpShark helps to incorporate the date and form into blockchain and IPFS (Interplanetary File System) respectively for good and guarantees its immutability.",
                "What is this? What should I do with this thing?",
                "SharpShark helps to incorporate the date and form into blockchain and IPFS (Interplanetary File System) respectively for good and guarantees its immutability."
            ]
        },
        "proof_autorship": "Proof-of-Authorship WEB3 Standard",
        "credit_founded": "Founded by",
        "credit_founded_names": "Sasha Ivanova & Valeriia Panina",
        "credit_founded_names_mobile": "Sasha Ivanova,\nValeriia Panina",
        "credit_powered": "Powered by<|>CopyLeaks",
        "history": {
            "error": "An error occurred while loading your certificate. Please, try again",
            "current_version": "Current version",
            "current_version_mobile": "Versions. Current",
            "title": "Title",
            "version_date_title": "Version No., Date",
            "version_date_title_mobile": "Title, version No., Date",
            "version_status_never_locked": "v.{{version}}",
            "version_status_locked": "v.{{version}}, locked",
            "version_status_unlocked": "v.{{version}}, unlocked",
            "go_back": "Go back",
            "other_versions": "Other versions ({{count}})",
            "other_versions_mobile": "Other ({{count}})",
            "check": "Check",
            "copy_password": "Copy password",
            "version": "v.{{version}}",
            "status_locked": "Locked",
            "status_unlocked": "Unlocked",
            "not_found": "Certificate not found. Please check if the URL is correct"
        }
    },
    "welcome": {
        "title_1": "Welcome to",
        "title_2": "Be respected with",
        "title_3": "Detect with",
        "description_1": "SharpShark helps to protect original content, monitor its misuse, and resolve disputes",
        "description_2": "You can receive a digital certificate for your content that will exist forever and can serve as proof of your authorship",
        "description_3": "You'll be able to distribute your content as you like: for free, for a fee, and even resolve cases with Shark if there’a misuse of it"
    }
}