import { fetchDisputesList } from "api/disputes";
import { useQuery } from "react-query";

const key = "DRAFT"
// const status = "Draft: Website Owner,Draft: Provider,Draft: Google"

export const useDisputeDraftList = ( search: string, sort: any[], skip: number = 0 , status: string) => {

    const sortOptions:any = []
    sort?.forEach((elem) => {
        const value = [elem.name, elem.value ? 1 : -1]
        sortOptions.push(value)
    })
  const { data: disputesList, refetch, isLoading } = useQuery([`DISPUTES_LIST_${key}`, skip, search, sortOptions, status], () => fetchDisputesList(status, skip * 15, search, sortOptions), {
    retry: 0,
    keepPreviousData: true
  })

  const pages = disputesList?.count ? Math.ceil(disputesList.count / 15) : 0
  const count = disputesList?.count

  return { disputesList, count, pages, isLoading, refetch }
}