import { DashboardLayout } from "features/Layouts/DashboardLayout";
import DisputListPage from "pages/disputes";
import DisputUpdatedPage from "pages/disputes/update";
import { LocalesListPage } from "pages/locales";
import { LocalePage } from "pages/locales/[locale]";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import { MintsListPage } from "../../pages/mints";
import { PurchasesListPage } from "../../pages/purchases";
import DisputesDraftTable from "../DisputeDraft/components/DisputesDraftTable";
import DisputeProgressTable from "../DistuteProgress/components/DisputeProgressTable";
import DisputeClaimTable from "../DisputeClaim/components/DisputeClaimTable";
import DisputeEndedTable from "../DisputeEnded/components/DisputeEndedTable";
import DisputeLetterTable from "../DisputeLetterSent/components/DisputeLetterTable";


export default function SecureRoutes() {
    return (
        <Routes>
            <Route path="/" element={<DashboardLayout />}>
                <Route index element={<Navigate replace to="/locales" />} />

                <Route path="locales" element={<Outlet />}>
                    <Route index element={<LocalesListPage />} />
                    <Route path=":localeName" element={<LocalePage />} />
                </Route>
                <Route path="mints" element={<Outlet />}>
                    <Route index element={<MintsListPage />} />
                </Route>
                <Route path="purchases" element={<Outlet />}>
                    <Route index element={<PurchasesListPage />} />
                </Route>
                <Route path="disputes" element={<Outlet />}>
                    <Route index element={<DisputListPage />} />
                    <Route path="" element={<DisputListPage />}>
                        <Route path="draft-website-owner" element={<DisputesDraftTable />} />
                        <Route path="draft-provider" element={<DisputesDraftTable />} />
                        <Route path="draft-google" element={<DisputesDraftTable />} />
                        
                        <Route path="in-progress-website-owner" element={<DisputeProgressTable />} />
                        <Route path="in-progress-provider" element={<DisputeProgressTable />} />
                        <Route path="in-progress-google" element={<DisputeProgressTable />} />
                        
                        <Route path="claim-sent-website-owner" element={<DisputeClaimTable />}/>
                        <Route path="claim-sent-provider" element={<DisputeClaimTable />}/>
                        <Route path="claim-sent-google" element={<DisputeClaimTable />}/>

                        <Route path="letter-sent-website-owner" element={<DisputeLetterTable />}/>
                        <Route path="letter-sent-provider" element={<DisputeLetterTable />}/>
                        <Route path="letter-sent-google" element={<DisputeLetterTable />}/>
                        
                        <Route path="ended" element={<DisputeEndedTable/>} />
                    </Route>
                    <Route path=":disputId" element={<DisputUpdatedPage />}>
                        <Route path="collecting-evidence" element={<DisputUpdatedPage />} />
                        <Route path="generating-claim" element={<DisputUpdatedPage />} />
                        <Route path="sending" element={<DisputUpdatedPage />} />
                        <Route path="notify-users" element={<DisputUpdatedPage />} />
                    </Route>
                </Route>
            </Route>
            <Route path="*" element={<Navigate replace to="/locales" />} />
        </Routes>
    );
}

