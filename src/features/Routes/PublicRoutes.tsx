import { AuthLayout } from "features/Layouts/AuthLayout";
import { LoginPage } from "pages/auth/login";
import { Navigate, Route, Routes } from "react-router-dom";

export default function PublicRoutes() {
    return (
        <Routes>
            <Route path="/auth" element={<AuthLayout />}>
                <Route index element={<Navigate replace to="/auth/login" />} />    

                <Route path="login" element={<LoginPage />} />
            </Route>
            <Route path="*" element={<Navigate replace to="/auth" />} />
        </Routes>
    );
}
