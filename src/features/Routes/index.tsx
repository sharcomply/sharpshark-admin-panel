import { useAuthStore } from "features/Auth/hooks/useAuthStore";
import { useAuthCheck } from "features/Auth/hooks/useAuthCheck";
import { useMount } from "hooks/useMount";

import PublicRoutes from "./PublicRoutes";
import SecureRoutes from "./SecureRoutes";
import { useState } from "react";

export default function AppRouter() {
    const authStore = useAuthStore();
    const { checkAuth } = useAuthCheck();
    const [render, setRender] = useState(false)

    useMount(() => checkAuth().finally(() => setRender(true)));

    if (!render) return null;

    return authStore.state.isLoggedIn ? <SecureRoutes /> : <PublicRoutes />;
}
