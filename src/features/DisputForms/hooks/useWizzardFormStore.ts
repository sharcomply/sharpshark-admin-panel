import { useCallback } from "react";
import { useAppDispatch, useAppSelector } from "store";
import { wizzardFromActions, DisputFormsState, } from "../store";

export function useWizzardFormStore() {
  const state = useAppSelector((state) => state.wizzardFrom);
  const dispatch = useAppDispatch()

  const setStep = useCallback((step: DisputFormsState["step"]) => {
    const action = wizzardFromActions.setStep(step);
    dispatch(action)
  }, [dispatch]);

  const clearStore = useCallback(() => {
    const action = wizzardFromActions.clear()
    dispatch(action)
  }, [dispatch]);

  const setDisputeStatus = useCallback((status: DisputFormsState["status"]) => {
    const action = wizzardFromActions.setDisputeStatus(status);
    dispatch(action);
  }, [dispatch])

  return {
    ...state,
      setDisputeStatus,
    actions: {
      clearStore,
      setStep,
      setDisputeStatus
    },
  };
}
