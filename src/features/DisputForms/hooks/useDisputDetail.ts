import { backend } from "api";
import { useQuery } from "react-query";

export function useDisputDetail(id: string) {
  const { data: disput, ...options } = useQuery(["DISPUT-ITEM", id], () => backend.disputes.fetchDisputDetail(id), {
    enabled: !!id,
    retry: false,
    refetchOnWindowFocus: false,
    keepPreviousData: true
  });

  return {
    disput,
    ...options,
  };
}