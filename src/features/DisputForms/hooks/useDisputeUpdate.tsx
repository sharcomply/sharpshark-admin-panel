import { backend } from "api";
import { editDisput } from "api/disputes";
import { useCallback, useState } from "react";
import {useDisputeStatus} from "./useDisputeStatus";

export const useDisputeUpdate = () => {
    const [loading, setLoading] = useState<boolean>(false);
    const status = useDisputeStatus();

    const handleUpdate = useCallback(async (form: EditDisput) => {
        try {
            setLoading(true);
            status.actions.update({ type: "success", message: "UPLOADING..." });
            await editDisput({
                _id: form._id,
                alertId: form.alertId,
                appearedOn: form.appearedOn,
                demands: form.demands,
                payAmount: form.payAmount,
                publicationQuestionLinks: form.publicationQuestionLinks,
                publicationQuestionWebarchiveLinks: form.publicationQuestionWebarchiveLinks,
                isPublicationQuestionWebarchiveFailed: form.isPublicationQuestionWebarchiveFailed,
                publicationLinks: form.publicationLinks,
                publicationWebarchiveLinks: form.publicationWebarchiveLinks,
                isPublicationWebarchiveFailed: form.isPublicationWebarchiveFailed,
                ownerClaimEmails: form.ownerClaimEmails,
                ownerClaimText: form.ownerClaimText,
                status: form.status,
            });

            const uploadFile = async () => {
                let draftsScreenshots: string[] = [];
                let publicationQuestionScreenshots: string[] = [];
                for (const item of form.draftsScreenshots!) {
                    if (typeof item !== "string") {
                        const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(form._id);
                        draftsScreenshots.push(downloadLink);
                        console.log(item);
                        await backend.disputes.uploadFile(uploadLink.url, item, uploadLink.fields);
                    } else draftsScreenshots.push(item);
                }
                for (const item of form.publicationQuestionScreenshots!) {
                    if (typeof item !== "string") {
                        const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(form._id);
                        publicationQuestionScreenshots.push(downloadLink);
                        await backend.disputes.uploadFile(uploadLink.url, item, uploadLink.fields);
                    } else publicationQuestionScreenshots.push(item);
                }
                return { draftsScreenshots, publicationQuestionScreenshots };
            };

            const downloadImg = async () => {
                const { draftsScreenshots, publicationQuestionScreenshots } = await uploadFile();
                console.log(draftsScreenshots);

                await editDisput({
                    ...form,
                    draftsScreenshots,
                    publicationQuestionScreenshots,
                });
            };

            downloadImg();

            // Get Links for Upload And Download
            // const { uploadLink, downloadLink } = await backend.disputes.getImageUploadLink(_id);

            // Upload File
            // await backend.drafts.uploadFile(uploadLink.url, payload.uploadFile!, uploadLink.fields);

            // // update draft using link to file
            // const draft = await update({
            //     _id,
            //     title: form.title,
            //     linkUrl: downloadLink,
            // });

            status.actions.update({ type: "success", message: "SAVED" })

            // // Redirect to created draft
            // await refetch();
            // navigate(`/drafts/${draft._id}`, { replace: true });
            // setLoading(false)
            setLoading(false);
        } catch (e) {
            status.actions.update({ type: "warn", message: "FAILED TO UPLOAD" })
            console.log("[DISPUTE_UPDATE]", e);
            setLoading(false);
        }
    }, []);

    return {
        handleUpdate,
        loading,
    };
};
