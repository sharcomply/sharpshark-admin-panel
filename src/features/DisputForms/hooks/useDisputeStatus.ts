import { useWizzardFormStore } from "./useWizzardFormStore";
import { useCallback } from "react";
import { DisputFormsState } from "../store";

export const useDisputeStatus = () => {
    const { status: state, setDisputeStatus } = useWizzardFormStore();

    const update = useCallback(
        (status: DisputFormsState["status"]) => {
            setDisputeStatus(status);
        },
        [setDisputeStatus]
    );

    return {
        state,
        actions: {
            update,
        },
    };
};
