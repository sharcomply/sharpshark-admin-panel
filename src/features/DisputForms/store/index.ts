import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Types
export interface DisputFormsState {
  step: number
  status: {
    type?: "error" | "warn" | "success";
    message?: string;
  };
}

// Initial State
const initialState: DisputFormsState = {
  step: 0,
  status: {
    type: undefined,
    message: undefined,
  },
};

// Slice
const wizzardFromSlice = createSlice({
  name: "wizzardForm",
  initialState,
  reducers: {
    clear: () => initialState,


    setStep: (state, action: PayloadAction<DisputFormsState["step"]>) => {
      state.step = action.payload;
    },
    setDisputeStatus: (state, action: PayloadAction<DisputFormsState["status"]>) => {
      state.status.type = action.payload.type;
      state.status.message = action.payload.message;
    },
  },
});

export const wizzardFromReducers = wizzardFromSlice.reducer;
export const wizzardFromActions = wizzardFromSlice.actions;