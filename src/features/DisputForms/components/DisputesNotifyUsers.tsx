import styled from "@emotion/styled";
import { Group, Stack, Text } from "@mantine/core";

import { useCallback } from "react";
import { useDisputeUpdate } from "../hooks/useDisputeUpdate";
import { SharkButton } from "components/SharkButton";

interface Props {
    getValues: (props?: string) => string;
}

export const NOTIFY_USERS_ALLOWED_STATUSES = ["Letter Sent: Website Owner", "Letter Sent: Provider", "In Progress: Google", "Claim Sent: Google", "Closed"];

const DisputesNotifyUsers = ({ getValues }: Props) => {
    const { handleUpdate } = useDisputeUpdate();
    const status = getValues("status");
    const checkStatus = useCallback((status: string) => {
        if (status === NOTIFY_USERS_ALLOWED_STATUSES[0]) {
            return "Claim Sent: Website Owner";
        }
        if (status === NOTIFY_USERS_ALLOWED_STATUSES[1]) {
            return "Claim Sent: Provider";
        }
        if (status === NOTIFY_USERS_ALLOWED_STATUSES[2]) {
            return "Claim Sent: Google";
        }

        return status;
    }, []);

    const onUpdateStatus = useCallback(async () => {
        await handleUpdate({ ...(getValues() as any), status: checkStatus(getValues("status")) });
    }, [handleUpdate, checkStatus, getValues]);

    return (
        <Root>
            <Wrapper>
                <Title>Change claim status: {checkStatus(status)}, and notify user about new status</Title>
                {!NOTIFY_USERS_ALLOWED_STATUSES.includes(status) && <Description>Send emails before changing status</Description>}
                <div style={{ maxWidth: 200 }}>
                    <SharkButton
                        type="button"
                        disabled={!NOTIFY_USERS_ALLOWED_STATUSES.includes(status)}
                        onClick={onUpdateStatus}
                        variant="filled"
                        buttonType="secondary"
                        size="small"
                        px={8}
                        py={4}
                    >
                        Notify
                    </SharkButton>
                </div>
            </Wrapper>

            <Footer>
                <Group>
                    <Text size={"sm"} weight={"400"} color={"#666892"}>
                        Document “Document long title, this is it’s long, long, very long title”
                    </Text>
                </Group>
            </Footer>
        </Root>
    );
};

export default DisputesNotifyUsers;

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    height: 100%;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin: 32px 32px 72px 48px;
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 16px;
    min-height: 56px;
    overflow: hidden;
    justify-content: space-between;
    flex-wrap: nowrap;
    align-items: center;
`;

const Title = styled.span`
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    margin-bottom: 16px;
`;

const Description = styled.span`
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: #666892;
    margin-bottom: 8px;
`;

