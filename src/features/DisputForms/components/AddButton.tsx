import styled from "@emotion/styled";
import addButton from "assets/images/add-button.svg";
import { ReactNode } from "react";

interface Props {
    children?: ReactNode;
    onClick?: () => void;
    disabled?: boolean;
}
const AddButton = ({ children, onClick, disabled }: Props) => {
    return (
        <>
            <Button type="button" disabled={disabled} onClick={onClick} img={addButton}>
                {children}
            </Button>
        </>
    );
};

export default AddButton;

const Button = styled.button<any>`
    display: flex;
    align-items: center;
    padding: 5px 8px;
    width: 155px;
    height: 30px;
    border: 1px solid #dddeee;
    border-radius: 4px;
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: #666892;
    cursor: pointer;
    &::before {
        content: "";
        background-image: url(${(props) => props.img});
        width: 13.33px;
        height: 13.33px;
        margin-right: 10px;
    }
`;
