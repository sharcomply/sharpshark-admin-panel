import styled from "@emotion/styled";
import removeButton from "assets/images/remove-button.svg";

interface Props {
    onClick?: (index?: number) => void;
    disabled?: boolean;
}

const RemoveButton = ({ onClick, disabled }: Props) => {
    return <Button disabled={disabled} onClick={onClick} img={removeButton} />;
};

export default RemoveButton;

const Button = styled.button<any>`
    width: 20px;
    height: 20px;
    background-image: url(${(props) => props.img});
`;
