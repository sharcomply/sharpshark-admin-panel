import styled from "@emotion/styled";
import { Group, Input, Stack, Text } from "@mantine/core";
import { editDisput, findEmails, sendEmails } from "api/disputes";
import { Icon } from "components/Icon";
import { SharkButton } from "components/SharkButton";
import { useCallback, useState } from "react";
import { Control, FieldValues, useFieldArray, UseFormRegister } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom";
import AddButton from "./AddButton";
import RemoveButton from "./RemoveButton";
import { useQueryClient } from "react-query";

interface Props {
    control: Control;
    register: UseFormRegister<FieldValues>;
    getValues: (props?: string) => string;
}
const checkStatus = (status: string) => {
    if (status.includes("Website Owner")) {
        return "Letter Sent: Website Owner";
    }
    if (status.includes("Provider")) {
        return "Letter Sent: Provider";
    }
    if (status.includes("Google")) {
        return "Letter Sent: Google";
    }
};

const DisputesSending = ({ control, register, getValues }: Props) => {
    const { disputId } = useParams();
    const [countFindEmail, setCountFindEmail] = useState<number | undefined>(undefined);
    const [countFindEmailProvider, setCountFindEmailProvider] = useState<number | undefined>(undefined);
    const [loading, setLoading] = useState(false);
    const [sendEmail, setSendEmail] = useState(false);
    const queryClient = useQueryClient();
    const navigate = useNavigate();
    const {
        fields: fieldsOwnerClaimEmails,
        append: appendOwnerClaimEmails,
        remove: removeOwnerClaimEmails,
    } = useFieldArray({
        control,
        name: "ownerClaimEmails",
    });

    const {
        fields: fieldsProviderClaimEmails,
        append: appendProviderClaimEmails,
        remove: removeProviderClaimEmails,
    } = useFieldArray({
        control,
        name: "providerClaimEmails",
    });

    const onFindEmails = useCallback(async () => {
        if (!disputId) return;
        try {
            setLoading(true);
            const response = await findEmails(disputId!);
            appendOwnerClaimEmails(response.emails);
            setCountFindEmail(response.emails.length);
            setLoading(false);
        } catch (error) {
            console.log(error);
            setLoading(false);
        }
    }, [disputId, appendOwnerClaimEmails]);

    const onFindEmailsProvider = useCallback(async () => {
        if (!disputId) return;
        try {
            setLoading(true);
            const response = await findEmails(disputId!);
            appendProviderClaimEmails(response.emails);
            setCountFindEmailProvider(response.emails.length);
            setLoading(false);
        } catch (error) {
            console.log(error);
            setLoading(false);
        }
    }, [disputId, appendOwnerClaimEmails]);

    const onSendEmail = useCallback(async () => {
        try {
            setLoading(true);
            const ownerClaimEmails = getValues("ownerClaimEmails") as any;
            await editDisput({ ...(getValues() as any), ownerClaimEmails });
            await sendEmails(disputId!);
            await editDisput({ _id: getValues("_id"), status: checkStatus(getValues("status")) });
            await queryClient.refetchQueries(["DISPUT-ITEM", disputId]);
            setSendEmail(true);
            setLoading(false);
        } catch (error) {
            console.log(error);
            setLoading(false);
        }
    }, [disputId, getValues]);

    const getClaimEmailsCount = () => {
        if (getValues("status") === "Draft: Website Owner" || getValues("status") === "In Progress: Website Owner") {
            return fieldsOwnerClaimEmails.length;
        }
        if (
            getValues("status") === "Claim Sent: Website Owner" ||
            getValues("status") === "Draft: Provider" ||
            getValues("status") === "In Progress: Provider"
        ) {
            return fieldsProviderClaimEmails.length;
        }

        return 0;
    };

    return (
        <Root>
            <Wrapper>
                {(getValues("status") === "Draft: Website Owner" ||
                    getValues("status") === "In Progress: Website Owner" ||
                    getValues("status") === "Letter Sent: Website Owner") && (
                    <>
                        <Title>Step 3. Sending the claim to the website owner</Title>
                        <div style={{ width: "380px" }}>
                            <div style={{ marginBottom: "32px" }}>
                                <div style={{ display: "flex", flexDirection: "column", marginBottom: "10px" }}>
                                    <SubTitle>contact email</SubTitle>
                                    <Description>
                                        Go to the contact section, view cookies and privacy policy, check the website on whois.com, or check their social media
                                        profiles. More tips on how to find contacts
                                    </Description>
                                    {fieldsOwnerClaimEmails.map((field, index) => (
                                        <div key={field.id} style={{ display: "flex", alignItems: "center", marginBottom: "8px" }}>
                                            <Input
                                                {...register(`ownerClaimEmails.${index}`)}
                                                style={{ marginRight: "10px", width: "350px", border: "1px solid #29CCB1", borderRadius: "4px" }}
                                            />
                                            <RemoveButton onClick={() => removeOwnerClaimEmails(index)} />
                                        </div>
                                    ))}
                                    <AddButton onClick={() => appendOwnerClaimEmails("")}>Add more</AddButton>
                                </div>
                            </div>
                        </div>
                        <Title>Find emails using a third-party service</Title>

                        <Description> If the platform finds emails, it will automatically add them to the form</Description>
                        <div style={{ marginBottom: "10px" }}>
                            <SharkButton px={8} py={4} size="small" variant="outlined" buttonType="secondary" onClick={onFindEmails} disabled={loading}>
                                {/* <Icon icon={`fluent:add-circle-16-filled`} color={"#666892"} /> */}
                                Find
                            </SharkButton>
                        </div>
                        {countFindEmail !== undefined && <Description style={{ color: "red" }}>{` Platform found ${countFindEmail} emails`}</Description>}
                    </>
                )}

                {(getValues("status") === "Claim Sent: Website Owner" ||
                    getValues("status") === "Draft: Provider" ||
                    getValues("status") === "In Progress: Provider" ||
                    getValues("status") === "Letter Sent: Provider") && (
                    <>
                        <Title>Step 3. Sending the claim to the provider</Title>
                        <Title>By email</Title>

                        <div style={{ marginBottom: "32px", width: "380px" }}>
                            <div style={{ display: "flex", flexDirection: "column" }}>
                                <SubTitle>contact email</SubTitle>
                                <Description>
                                    Their provider is [Provider’s name]. This is the email we found. You may add more contacts if you know any others.
                                </Description>
                                {fieldsProviderClaimEmails.map((field, index) => (
                                    <div key={field.id} style={{ display: "flex", alignItems: "center", marginBottom: "8px" }}>
                                        <Input
                                            {...register(`providerClaimEmails.${index}`)}
                                            variant="unstyled"
                                            style={{
                                                marginRight: "10px",
                                                width: "350px",
                                                border: "1px solid #29CCB1",
                                                borderRadius: "4px",
                                                paddingLeft: "12px",
                                            }}
                                        />
                                        <RemoveButton onClick={() => removeProviderClaimEmails(index)} />
                                    </div>
                                ))}

                                <AddButton onClick={() => appendProviderClaimEmails("")}>Add more</AddButton>
                            </div>
                        </div>
                        <Title>Find emails using a third-party service</Title>

                        <Description> If the platform finds emails, it will automatically add them to the form</Description>
                        <div style={{ marginBottom: "10px" }}>
                            <SharkButton px={8} py={4} size="small" variant="outlined" buttonType="secondary" onClick={onFindEmailsProvider} disabled={loading}>
                                {/* <Icon icon={`fluent:add-circle-16-filled`} color={"#666892"} /> */}
                                Find
                            </SharkButton>
                        </div>
                        {countFindEmailProvider !== undefined && (
                            <Description style={{ color: "red" }}>{` Platform found ${countFindEmailProvider} emails`}</Description>
                        )}
                    </>
                )}
                {(getValues("status") === "Claim Sent: Provider" ||
                    getValues("status") === "Draft: Google" ||
                    getValues("status") === "In Progress: Google" ||
                    getValues("status") === "Claim Sent: Google" ||
                    getValues("status") === "Under Court" ||
                    getValues("status") === "Closed") && (
                    <>
                        <Title>Step 3. Sending the claim to Google law office</Title>
                        <div style={{ width: "380px" }}>
                            <div style={{ marginBottom: "16px" }}>
                                <div style={{ display: "flex", flexDirection: "column" }}>
                                    <SubTitle>at First, </SubTitle>
                                    <Description>Log into your Google account and follow the link:</Description>
                                </div>
                            </div>
                            <div style={{ marginBottom: "16px" }}>
                                <div style={{ display: "flex", alignItems: "center" }}>
                                    <Icon icon={"ic:link"} color={"#979CBC"} rotate={-30} style={{ marginRight: "8px" }} />
                                    <a
                                        href="https://www.google.com/webmasters/tools/legal-removal-request?complaint_type=dmca&visit_id=637932436777923240-611449655&hl=en&rd=1"
                                        style={{
                                            marginRight: "8px",
                                            fontWeight: "400",
                                            fontSize: "16px",
                                            lineHeight: "20px",
                                            color: "#4700E5",
                                        }}
                                    >
                                        {" "}
                                        Legal removal request{" "}
                                    </a>
                                    <div
                                        style={{
                                            border: "1px solid #DDDEEE",
                                            borderRadius: "4px",
                                            padding: "5px",
                                        }}
                                    >
                                        <Icon icon={"ic:open-in-new"} color={"#666892"} />
                                    </div>
                                </div>
                            </div>
                            <div style={{ marginBottom: "16px" }}>
                                <div style={{ display: "flex", flexDirection: "column" }}>
                                    <SubTitle>Then </SubTitle>
                                    <Description>Identify and describe the copyrighted material,</Description>
                                </div>
                            </div>
                            <div style={{ marginBottom: "16px" }}>
                                <div style={{ display: "flex", flexDirection: "column" }}>
                                    <SubTitle>And last </SubTitle>
                                    <Description>And tell us you did it!</Description>
                                </div>
                            </div>
                        </div>
                    </>
                )}
            </Wrapper>

            <Footer>
                <Group>
                    <Text size={"sm"} weight={"400"} color={"#666892"}>
                        Document “Document long title, this is it’s long, long, very long title”
                    </Text>
                </Group>

                {(getValues("status") === "Claim Sent: Website Owner" ||
                    getValues("status") === "Draft: Provider" ||
                    getValues("status") === "In Progress: Provider" ||
                    getValues("status") === "Draft: Website Owner" ||
                    getValues("status") === "In Progress: Website Owner") && (
                    <div>
                        {getClaimEmailsCount() > 0 ? (
                            <Description style={{ maxWidth: "60%" }}>
                                Send letters to receivers from list of contact emails ({getClaimEmailsCount()})
                            </Description>
                        ) : null}
                        <div style={{ display: "flex", alignItems: "center", marginTop: 8 }}>
                            <SharkButton
                                px={8}
                                py={4}
                                size="small"
                                variant="outlined"
                                buttonType="secondary"
                                onClick={onSendEmail}
                                disabled={loading || getClaimEmailsCount() < 1}
                                type="button"
                            >
                                Send letters
                            </SharkButton>
                            {sendEmail && (
                                <div>
                                    <Description style={{ color: "rgb(41, 204, 177)", marginLeft: "10px" }}>Done</Description>
                                </div>
                            )}
                        </div>
                    </div>
                )}
                {getValues("status") === "In Progress: Google" ? (
                    <SharkButton
                        type="button"
                        onClick={() => navigate(window.location.pathname.replace(/[^/]*$/, "notify-users"), { replace: true })}
                        variant="filled"
                        buttonType="secondary"
                        size="small"
                        px={8}
                        py={4}
                    >
                        Continue
                    </SharkButton>
                ) : null}
            </Footer>
        </Root>
    );
};

export default DisputesSending;

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin: 32px 32px 72px 48px;
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 16px;
    min-height: 56px;
    overflow: hidden;
    justify-content: space-between;
    flex-wrap: nowrap;
    align-items: center;
`;

const Title = styled.span`
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    margin-bottom: 16px;
`;

const SubTitle = styled.span`
    font-weight: 700;
    font-size: 12px;
    line-height: 16px;
    text-transform: uppercase;
    margin-bottom: 6px;
`;

const Description = styled.span`
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: #666892;
    margin-bottom: 8px;
`;

