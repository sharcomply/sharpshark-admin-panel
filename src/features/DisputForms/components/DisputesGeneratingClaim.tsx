import styled from "@emotion/styled";
import { Group, Stack, Text } from "@mantine/core";
import { SharkButton } from "components/SharkButton";
import { Control, Controller } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { RichTextEditor } from "@mantine/rte";

interface Props {
    control: Control;
    getValues: (props?: string) => string;
}

const DisputesGeneratingClaim = ({ control, getValues }: Props) => {
    const navigate = useNavigate();
    const status = getValues("status");
    const [disabled, setDisable] = useState(false);
    useEffect(() => {
        if (status) {
            if (status === "Closed" || status === "Under Court") {
                setDisable(true);
            } else {
                setDisable(false);
            }
        }
    }, [status]);

    return (
        <Root>
            <Wrapper>
                {(getValues("status") === "Draft: Website Owner" || getValues("status") === "In Progress: Website Owner") && (
                    <>
                        <Title>Step 2. Generating your claim to the website owner</Title>

                        <Controller
                            name="ownerClaimText"
                            control={control}
                            render={({ field }) => (
                                <TextArea
                                    controls={[]}
                                    py={8}
                                    px={16}
                                    id="text"
                                    {...field}
                                    spellCheck={false}
                                    value={field.value}
                                    readOnly={disabled}
                                    // onChange={(e) => handleText(String(e.target.value))}
                                    placeholder="Enter Text"
                                    styles={{
                                        root: { flex: 1 },
                                    }}
                                />
                            )}
                        />
                    </>
                )}

                {(getValues("status") === "Claim Sent: Website Owner" ||
                    getValues("status") === "Draft: Provider" ||
                    getValues("status") === "In Progress: Provider") && (
                    <>
                        <Title>Step 2. Preparing your claim for the provider</Title>

                        <Controller
                            name="providerClaimText"
                            control={control}
                            render={({ field }) => (
                                <TextArea
                                    controls={[]}
                                    py={8}
                                    px={16}
                                    id="text"
                                    readOnly={disabled}
                                    spellCheck={false}
                                    {...field}
                                    value={field.value}
                                    // onChange={(e) => handleText(String(e.target.value))}
                                    placeholder="Enter Text"
                                    styles={{
                                        root: { flex: 1 },
                                    }}
                                />
                            )}
                        />
                    </>
                )}

                {(getValues("status") === "Claim Sent: Provider" ||
                    getValues("status") === "Draft: Google" ||
                    getValues("status") === "In Progress: Google" ||
                    getValues("status") === "Claim Sent: Google" ||
                    getValues("status") === "Under Court" ||
                    getValues("status") === "Closed") && (
                    <>
                        <Title>Step 2. Preparing your legal reguest for Google</Title>

                        <Controller
                            name="googleClaimText"
                            control={control}
                            render={({ field }) => (
                                <TextArea
                                    controls={[]}
                                    id="text"
                                    {...field}
                                    value={field.value}
                                    readOnly={disabled}
                                    spellCheck={false}
                                    // onChange={(e) => handleText(String(e.target.value))}
                                    placeholder="Enter Text"
                                    styles={{
                                        root: { flex: 1 },
                                    }}
                                />
                            )}
                        />
                    </>
                )}
            </Wrapper>

            <Footer>
                <Group>
                    <Text size={"sm"} weight={"400"} color={"#666892"}>
                        Document “Document long title, this is it’s long, long, very long title”
                    </Text>
                </Group>
                <Group spacing={12}>
                    <Group spacing={8}></Group>

                    <SharkButton
                        onClick={() => navigate(window.location.pathname.replace(/[^/]*$/, "sending"), { replace: true })}
                        variant="filled"
                        buttonType="secondary"
                        size="small"
                        px={8}
                        py={4}
                    >
                        Seems good, next
                    </SharkButton>
                </Group>
            </Footer>
        </Root>
    );
};

export default DisputesGeneratingClaim;

const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
`;

const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    margin: 32px 32px 72px 48px;
`;

const Footer = styled(Group)`
    position: relative;
    background-color: #ffffff;
    border-top: 1px solid #dddeee;
    border-bottom-left-radius: 8px;
    border-bottom-right-radius: 8px;
    padding: 14px 16px;
    height: 56px;
    overflow: hidden;
    justify-content: space-between;
`;

const Title = styled.span`
    font-weight: 700;
    font-size: 20px;
    line-height: 24px;
    margin-bottom: 16px;
`;

const TextArea = styled(RichTextEditor)`
    border: 1px solid #dddeee;
    border-radius: 8px;

    & .mantine-RichTextEditor-toolbar {
        display: none;
    }

    #text {
        padding: 8px 16px;
        font-weight: 400;
        font-size: 16px;
        line-height: 20px;
        color: #0a001e;
    }

    #text::placeholder {
        font-weight: 400;
        font-size: 16px;
        line-height: 20px;
        color: #666892;
    }
`;

