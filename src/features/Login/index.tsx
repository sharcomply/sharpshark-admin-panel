import { Stack, TextInput, PasswordInput } from "@mantine/core";
import { Button } from "components/Button";
import { useCallback, useState } from "react";
import { useLogin } from "./hooks/useLogin";
import { LoginFormType, useLoginForm } from "./hooks/useLoginForm";

export function LoginForm() {
    const { login } = useLogin();
    const form = useLoginForm();

    const [loading, setLoading] = useState(false);

    const onLogin = useCallback(
        (values: LoginFormType) => {
            setLoading(true);
            login(values, form).finally(() => setLoading(false));
        },
        [login, form]
    );

    return (
        <form onSubmit={form.onSubmit(onLogin)}>
            <Stack spacing={"md"}>
                <TextInput label="Email" placeholder="example@example.com" required {...form.getInputProps("email")} />
                <PasswordInput label="Password" placeholder="Your password" required {...form.getInputProps("password")} />
            </Stack>
            <Button height={42} type="submit" width={"100%"} mt={"xl"} color="primary" variant="filled" loading={loading}>
                Sign In
            </Button>
        </form>
    );
}
