import { backend } from "api";
import { useAuthStore } from "features/Auth/hooks/useAuthStore";
import { useAuthCheck } from "features/Auth/hooks/useAuthCheck";
import { setToken } from "helpers/tokenStorage";
import { useCallback } from "react";
import { useMutation } from "react-query";
import { UseFormReturnType } from "@mantine/form/lib/use-form";

export function useLogin() {
    const authStore = useAuthStore();
    const { checkAuth } = useAuthCheck();

    const loginRequest = useMutation((data: LoginParams) => backend.auth.login(data));

    const login = useCallback(
        async (
            values: LoginParams,
            form: UseFormReturnType<{
                email: string;
                password: string;
            }>
        ) => {
            if (loginRequest.isLoading || authStore.state.isLoggedIn) return;

            try {
                const token = await loginRequest.mutateAsync(values);
                if (!token) throw String("Login Failed");

                setToken(token);
                await checkAuth();
            } catch (err) {
                console.log(err);
                if (err === "Login Failed") {
                    form.setErrors({
                        email: " ",
                        password: "Invalid email or password",
                    });
                }
            }
        },
        [authStore.state.isLoggedIn, checkAuth, loginRequest]
    );

    return {
        login,
    };
}
