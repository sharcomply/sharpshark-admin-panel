import { useForm, yupResolver } from "@mantine/form";
import { object, string } from "yup";

export type LoginFormType = {
    email: string;
    password: string;
}

export const loginSchema = object().shape({
    email: string().required().email().lowercase(),
    password: string().required(),
});

export function useLoginForm() {
    const form = useForm({
        schema: yupResolver(loginSchema),
        initialValues: {
            email: "",
            password: "",
        },
    });

    return form;
}
