import {Group, Text} from "@mantine/core";
import {humanizeDate} from "helpers/humanizeDate";
import styled from "@emotion/styled";
import {useEffect, useState} from "react";
import {Icon} from "@iconify/react";
import {Link} from "react-router-dom";
import {SharkButton} from "components/SharkButton";

// Props
type ComponentProps = {
    item: any[];
};

// Component
export const DisputeEndedItem = ({item}: ComponentProps) => {
    const [opened, setOpen] = useState(false);

    const [itemAlert, setItemAlert] = useState<Disput[]>([]);
    const [firstItemAlert, setFirstItemAlert] = useState<Disput>();

    useEffect(() => {
        if (!item) return;
        setItemAlert(item);
    }, [item, setItemAlert]);

    useEffect(() => {
        setFirstItemAlert(itemAlert[0]);
    }, [itemAlert, setFirstItemAlert]);


    if (!itemAlert.length || !firstItemAlert) return null;

    return (
        <>
            {itemAlert.length === 1 && (
                <>
                    <Root>
                        <td style={{overflow: "hidden"}}>
                            <tr style={{overflow: "hidden", width: "100%"}}>
                                <td style={{paddingLeft: "48px"}}>
                                    {/*<ViewedStatus isRead={true} length={itemAlert.length} />*/}
                                </td>
                                <td>
                                    <Group px={16} align="center" sx={{height: 54}} style={{overflow: "hidden"}}>
                                        <Text
                                            size={"sm"}
                                            color={"#0A001E"}
                                            weight={"500"}
                                            style={{
                                                overflow: "hidden",
                                                textOverflow: "ellipsis",
                                                whiteSpace: "nowrap",
                                            }}
                                        >
                                            {firstItemAlert.documentId.title}
                                        </Text>
                                    </Group>
                                </td>
                                <td>
                                    <Group px={16} align="center" sx={{height: 54}}>
                                        <Text
                                            size={"sm"}
                                            color={"#666892"}
                                            weight={"500"}
                                            style={{
                                                overflow: "hidden",
                                                textOverflow: "ellipsis",
                                                whiteSpace: "nowrap",
                                                width: "45%",
                                            }}
                                        >
                                            {/* {firstItemAlert.introduction} */}
                                        </Text>
                                    </Group>
                                </td>
                            </tr>
                        </td>
                        <td>
                            {/* <a href={firstItemAlert.isSimilarContent ? firstItemAlert.url : "#"}> */}
                            <Group px={16} align="center" sx={{height: 54}} style={{overflow: "hidden"}}>
                                <Text
                                    size={"sm"}
                                    color={"#666892"}
                                    weight={"500"}
                                    style={{
                                        overflow: "hidden",
                                        textOverflow: "ellipsis",
                                        whiteSpace: "nowrap",
                                    }}
                                >
                                    {/*{firstItemAlert.appearedOn}*/}
                                </Text>
                            </Group>
                            {/* </a> */}
                        </td>
                        <td>
                            <Group px={16} align="center" sx={{height: 54}} style={{overflow: "hidden"}}>
                                <Text size={"sm"} color={"#666892"} weight={"500"}>
                                    {humanizeDate(firstItemAlert.createdAt).shortDate}
                                </Text>
                            </Group>
                        </td>

                        <td>
                            <Group px={16} position="center" align="center" sx={{height: 54}} style={{overflow: "hidden"}}>
                                <Text size={"sm"} color={"#666892"} weight={"500"}>
                                    {humanizeDate(item[0].updatedAt).shortDate}
                                </Text>
                            </Group>
                        </td>
                        <td>
                            <Group px={16} position="center" align="center" sx={{height: 80}}>
                                <Link to={`/disputes/${firstItemAlert._id}/collecting-evidence`}>
                                    <SharkButton
                                        variant={"filled"}
                                        buttonType="primary"
                                        // onClick={() => onSaveAlertParam(firstItemAlert.appearedOn)}
                                        width={"100%"}
                                        style={{padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer"}}
                                    >
                                        React...
                                    </SharkButton>
                                </Link>
                            </Group>
                        </td>
                        <td>
                        </td>
                    </Root>
                </>
            )}
            {itemAlert.length > 1 && (
                <>
                    <Root>
                        <>
                            <td colSpan={4} style={{overflow: "hidden"}}>
                                <tr onClick={() => setOpen(!opened)} style={{cursor: "pointer"}}>
                                    <td>
                                        <Group px={16} align="center" sx={{height: 54}}>
                                            {opened && <Icon icon={`ic:round-keyboard-arrow-down`} rotate={2} color="#4b506d"/>}
                                            {!opened && <Icon icon={`ic:round-keyboard-arrow-down`} color="#4b506d"/>}
                                        </Group>
                                    </td>
                                    <td style={{overflow: "hidden"}}>
                                        <Group px={16} align="center" direction="row" sx={{height: 54}} style={{overflow: "hidden"}}>
                                            <Text
                                                size={"sm"}
                                                color={"#0A001E"}
                                                weight={"500"}
                                                style={{
                                                    overflow: "hidden",
                                                    textOverflow: "ellipsis",
                                                    whiteSpace: "nowrap",
                                                }}
                                            >
                                                {firstItemAlert.documentId.title}
                                            </Text>
                                        </Group>
                                    </td>
                                    <td>
                                        <Group px={16} align="center" sx={{height: 54}}>
                                            <Text
                                                size={"sm"}
                                                color={"#666892"}
                                                weight={"500"}
                                                style={{
                                                    overflow: "hidden",
                                                    textOverflow: "ellipsis",
                                                    whiteSpace: "nowrap",
                                                    width: "90%",
                                                }}
                                            >
                                                {/* {firstItemAlert.introduction} */}
                                            </Text>
                                        </Group>
                                    </td>
                                </tr>
                            </td>
                        </>

                        <td>

                        </td>
                    </Root>

                    {opened &&
                        item.map((alert, index) => (
                            <>
                                <Root key={index}>
                                    <td></td>
                                    <td>
                                        {/* <a href={firstItemAlert.isSimilarContent ? firstItemAlert.url : "#"}> */}
                                        <Group px={16} align="center" sx={{height: 54}} style={{overflow: "hidden"}}>
                                            <Text
                                                size={"sm"}
                                                color={"#666892"}
                                                weight={"500"}
                                                style={{
                                                    overflow: "hidden",
                                                    textOverflow: "ellipsis",
                                                    whiteSpace: "nowrap",
                                                }}
                                            >
                                                {alert.appearedOn}
                                            </Text>
                                        </Group>
                                        {/* </a> */}
                                    </td>

                                    <td>
                                        <Group px={16} align="center" sx={{height: 54}} style={{overflow: "hidden"}}>
                                            <Text size={"sm"} color={"#666892"} weight={"500"}>
                                                {humanizeDate(alert.createdAt).shortDate}
                                            </Text>
                                        </Group>
                                    </td>
                                    <td>
                                        <Group px={16} position="center" align="center" sx={{height: 54}} style={{overflow: "hidden"}}>
                                            <Text size={"sm"} color={"#666892"} weight={"500"}>
                                                {humanizeDate(alert.updatedAt).shortDate}
                                            </Text>
                                        </Group>
                                    </td>
                                    <td>
                                        <Group px={16} position="center" align="center" sx={{height: 80}}>
                                            <SharkButton
                                                as={Link}
                                                to={`/disputes/${alert._id}/collecting-evidence`}
                                                variant={"filled"}
                                                buttonType="primary"
                                                width={"100%"}
                                                style={{padding: "3px", paddingLeft: "10px", paddingRight: "10px", cursor: "pointer"}}
                                            >
                                                React...
                                            </SharkButton>
                                        </Group>
                                    </td>
                                    <td>
                                    </td>
                                </Root>
                            </>
                        ))}
                </>
            )}
        </>
    );
};

// Styling
const Root = styled.tr`
    transition: background-color 200ms linear;
    border-top: 1px solid #dddeee;

    &:hover {
        background-color: #edeeff;
    }
`;
