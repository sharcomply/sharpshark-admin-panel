import {createStyles, LoadingOverlay, Pagination, Stack, Text, TextInput} from "@mantine/core";
import {TableHead} from "../../../components/TableHead";
import {MintListItem} from "./MintListItem";
import {Fragment, useCallback, useState} from "react";
import {useMints} from "../hooks/useMints";
import {sortsTypes} from "../../../helpers/sortConstants";

const defaultSorts = [[]]

export const MintList = () => {
    const [currentPage, setCurrentPage] = useState(1)
    const [search, setSearch] = useState("")
    const [sortOptions, setSortOptions] = useState(defaultSorts)
    const {data: mints, pages, isLoading, refetch} = useMints(currentPage - 1, search, sortOptions)
    const {classes} = useStyles();

    const handleChangeSort = useCallback(async (values: any) => {
        setSortOptions(values)
        await refetch()
    }, [setSortOptions])

    const handleSearch = useCallback(async (value: string) => {
        setSearch(value)
        setCurrentPage(1)
    }, [setSearch, setCurrentPage])

    return (
        <Stack className={classes.stack}>
            <Stack align={'flex-end'} m={8}>
                <TextInput placeholder={"Search"} value={search} onChange={(value) => handleSearch(value.target.value)}/>
            </Stack>

            <table className={classes.table}>

                <TableHead change={handleChangeSort} items={[
                    {title: 'Email', field: "email", value: false, sortType: sortsTypes.string, width: '25%'},
                    {title: 'Wallet Address', field: "walletAddress", sortType: sortsTypes.string, value: false, width: '35%'},
                    {title: 'Token ID', field: "tokenId", value: false, sortType: sortsTypes.string, width: '35%'},
                    {title: 'Created At', field: "createdAt", value: false, sortType: sortsTypes.date, width: '20%'},
                ]} sortOptions={sortOptions}/>

                <tbody style={{position: 'relative'}}>
                <tr>
                    <td>
                        <LoadingOverlay visible={isLoading}/>
                    </td>
                </tr>
                <Fragment>
                    {isLoading && Array.from({length: 15}).map((_, index) => (
                        <MintListItem
                            key={`${index}`}
                            email={''}
                            address={''}
                            createdAt={''}
                            tokenId={''}
                        />
                    ))

                    }
                    {mints?.items?.map((mint, index) => (
                        <MintListItem
                            key={`${index}`}
                            email={mint.email}
                            address={mint.walletAddress}
                            createdAt={mint.createdAt}
                            tokenId={mint.tokenId}
                        />
                    ))}

                </Fragment>
                {!isLoading && !mints?.count && (
                    <tr style={{borderTop: '1px solid #DDDEEE'}}>
                        <td style={{padding: '16px'}}>
                            <Text size={'sm'} color={'#666892'} weight={'400'}>This is a blank page... Cases will appear here</Text>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                )}
                </tbody>
            </table>
            <Stack align={'center'} style={{marginBlock: '16px'}}>
                {pages > 1 &&
                    <Pagination style={{margin: '0 auto'}} size="lg" total={pages} page={currentPage} onChange={setCurrentPage}/>
                }
            </Stack>
        </Stack>
    )
}


const useStyles = createStyles(() => ({
    stack: {
        backgroundColor: "#ffffff",
        border: "1px solid #dddeee",
        borderRadius: "8px",
        margin: "40px",
        marginTop: "65px",
    },
    table: {
        width: "100%",
        tableLayout: "fixed",
        borderCollapse: "collapse",
        overflowWrap: "anywhere",
    }
}));


