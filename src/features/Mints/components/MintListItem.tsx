import {createStyles, Group, Text} from "@mantine/core";
import dayjs from "dayjs";

type componentProps = {
    email: string,
    address: string,
    tokenId: string,
    createdAt: string
}

export const MintListItem = ({email, address, tokenId, createdAt}: componentProps) => {
    const createdDate = dayjs(createdAt).format("DD/MM/YYYY HH:mm")
    const {classes} = useStyles()
    return (
        <tr className={classes.tr}>
            <td style={{width: '20%'}}>
                <Group px={16} align="center" sx={{height: 54}}>
                    <Text
                        size={"sm"}
                        color={"#0A001E"}
                        weight={"500"}
                        style={{
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            whiteSpace: "nowrap",
                        }}
                    >
                        {email ? email : 'EMAIL NOT FOUND'}
                    </Text>
                </Group>
            </td>
            <td style={{width: '30%'}}>
                <Group px={16} align="center" sx={{height: 54}}>
                    <Text
                        size={"sm"}
                        color={"#0A001E"}
                        weight={"500"}
                        style={{
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            whiteSpace: "nowrap",
                        }}
                    >
                        {address}
                    </Text>
                </Group>
            </td>
            <td style={{width: '30%'}}>
                <Group px={16} align="center" sx={{height: 54}}>
                    <Text
                        size={"sm"}
                        color={"#0A001E"}
                        weight={"500"}
                        style={{
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            whiteSpace: "nowrap",
                        }}
                    >
                        {tokenId}
                    </Text></Group>

            </td>
            <td style={{width: '20%'}}>
                <Group px={16} align="center" sx={{height: 54}}>
                    <Text
                        size={"sm"}
                        color={"#0A001E"}
                        weight={"500"}
                        style={{
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            whiteSpace: "nowrap",
                        }}
                    >
                        {createdDate}
                    </Text>
                </Group>

            </td>

        </tr>
    );
};

const useStyles = createStyles(() => ({
    tr: {
        transition: "background-color 200ms linear",
        borderTop: "1px solid #dddeee",
        '&:hover': {
            backgroundColor: "#edeeff"
        }
    },
}));

