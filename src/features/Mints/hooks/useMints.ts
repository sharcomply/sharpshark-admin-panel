import {useQuery} from "react-query";
import {fetchMints} from "../../../api/mints";

export const useMints = (skip:number, search?: string, sort?: any[]) => {
    const sortOptions:any = []
    sort?.forEach((elem) => {
        const value = [elem.name, elem.value ? 1 : -1]
        sortOptions.push(value)
    })

    const {data, isLoading, refetch} = useQuery([`MINTS_LIST`, skip, search, sortOptions], () => fetchMints(skip * 15, search || "", sortOptions), {keepPreviousData: false, })
    const pages = data?.count ? Math.ceil(data.count / 15) : 0
    return {
        data,
        pages,
        isLoading,
        refetch
    }
}