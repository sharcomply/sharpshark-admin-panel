import { getToken, removeToken } from "helpers/tokenStorage";
import { useCallback } from "react";
import { useAuthStore } from "./useAuthStore";
import { useWhoAmi } from "./useWhoAmi";

export function useAuthCheck() {
    const { refetch } = useWhoAmi();
    const authStore = useAuthStore();

    const checkAuth = useCallback(async () => {
        if (!getToken()) return;

        try {
            await refetch();
            authStore.actions.login(true);
            return true;
        } catch (err) {
            console.log(err);
            authStore.actions.logout();
            removeToken();
            return false;
        }
    }, [authStore.actions, refetch]);

    return {
        checkAuth,
    };
}
