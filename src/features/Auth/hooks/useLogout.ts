import { removeToken } from "helpers/tokenStorage";
import { useCallback } from "react";
import { useAuthStore } from "./useAuthStore";

export function useLogout() {
    const authStore = useAuthStore();

    const logout = useCallback(() => {
        authStore.actions.logout();
        removeToken();
    }, [authStore.actions]);

    return {
        logout,
    };
}
