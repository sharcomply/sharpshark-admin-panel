
export interface NavItem {
    label: string;
    icon?: string;
    link: string;
    links?: NavLinkItem[];
    soon?: boolean
}

export interface NavLinkItem {
    label: string;
    link: string;
}
