import { NavItem } from "./interfaces/NavItem";

export const NavigationLinks: NavItem[] = [
    { 
        label: 'Locales', 
        link: '/locales',
    },
    { 
        label: 'Mints', 
        link: '/mints',
    },
    { 
        label: 'Purchases', 
        link: '/purchases',
    },
    { 
        label: 'Disputes', 
        link: '/disputes/draft-website-owner',
    }

];
