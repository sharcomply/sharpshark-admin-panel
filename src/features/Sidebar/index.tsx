import { Navbar, Group, ScrollArea, createStyles, Text, Stack } from "@mantine/core";
import { Logo } from "components/Icons/Logo";
import { NavigationLinks } from "./navigation.data";
import { LinkGroup } from "./components/LinkGroup";

// Styles
const useStyles = createStyles((theme) => ({
    navbar: {
        backgroundColor: theme.white,
    },

    header: {
        padding: theme.spacing.md,
        color: theme.colors.grayscale[9],
        borderBottom: `1px solid ${theme.colors.grayscale[2]}`,
    },

    links: {
        paddingLeft: theme.spacing.md,
        paddingRight: theme.spacing.md,
        paddingTop: theme.spacing.md,
        paddingBottom: theme.spacing.md,
    },
}));

// Component
export function Sidebar() {
    const { classes } = useStyles();

    const links = NavigationLinks.map((item) => <LinkGroup {...item} key={item.label} />);

    return (
        <Navbar width={{ sm: 300 }} className={classes.navbar}>
            <Navbar.Section className={classes.header}>
                <Group position="left" spacing={16} px={24}>
                    <Logo fontSize={32} />
                    <Text weight={"700"} size={"xl"}>SharpShark</Text>
                </Group>
            </Navbar.Section>

            <Navbar.Section grow className={classes.links} component={ScrollArea}>
                <Stack spacing={4}>
                    {links}
                </Stack>
            </Navbar.Section>
        </Navbar>
    );
}
