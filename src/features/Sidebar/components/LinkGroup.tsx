import React, { useState } from "react";
import { Box, Collapse, createStyles, Group, Text, ThemeIcon, UnstyledButton } from "@mantine/core";
import { NavItem } from "../interfaces/NavItem";
import { Link, useLocation } from "react-router-dom";
import { Icon } from "components";
import clsx from "clsx";
import { useWatchEffect } from "hooks/useWatchEffect";

// Styles
const useStyles = createStyles((theme) => ({
    control: {
        fontWeight: 500,
        display: "block",
        width: "100%",
        padding: `12px 16px`,
        color: theme.colors.grayscale[9],
        fontSize: theme.fontSizes.sm,
        borderRadius: 4,

        "&:hover": {
            backgroundColor: theme.colors.grayscale[1],
        },
    },

    disabled: {
        "&:hover": {
            backgroundColor: 'transparent',
        },
    },

    activeControl: {
        backgroundColor: theme.colors.grayscale[1],
    },

    link: {
        fontWeight: 500,
        display: "block",
        textDecoration: "none",
        padding: `12px 20px`,
        paddingLeft: 31,
        marginLeft: 26,
        fontSize: theme.fontSizes.sm,
        color: theme.colors.grayscale[7],
        borderLeft: `1px solid ${theme.colors.grayscale[3]}`,
        borderRadius: `0 4px 4px 0`,

        "&:hover": {
            backgroundColor: theme.colors.grayscale[1],
        },
    },

    chevron: {
        transition: "transform 200ms ease",
    },
}));

// Component
export function LinkGroup({ icon, link, label, links, soon }: NavItem) {
    const { classes, theme } = useStyles();
    const { pathname } = useLocation();

    const [opened, setOpened] = useState(false);

    const hasLinks = Array.isArray(links);
    const isActive = !!pathname.match(link);

    useWatchEffect(!!pathname.match(link), () => {
        setOpened(true);
    });

    // Nested Items
    const items = (hasLinks ? links : []).map((item) => (
        <Link to={`${link}${item.link}`} key={item.label}>
            <Text className={classes.link}>{item.label}</Text>
        </Link>
    ));

    const control = (
        <UnstyledButton onClick={() => setOpened((o) => !o)} className={clsx(classes.control, !hasLinks && isActive && classes.activeControl, soon && classes.disabled)}>
            <Group position="apart" spacing={0}>
                <Box sx={{ display: "flex", alignItems: "center" }}>
                    {icon && (
                        <ThemeIcon variant="light">
                            <Icon size={18} icon={icon} color={theme.colors.blue[4]} />
                        </ThemeIcon>
                    )}
                    <Group ml="md" spacing={4}>
                        <Text sx={{ fontSize: 16, lineHeight: "20px" }} weight="400" color={soon ? theme.colors.grayscale[6] : undefined}>
                            {label}
                        </Text>
                        {soon && (
                            <Text sx={{ fontSize: 12, lineHeight: "20px", fontStyle: "italic" }} weight="400" color={theme.colors.grayscale[6]}>
                                (Coming Soon)
                            </Text>
                        )}
                    </Group>
                </Box>
                {hasLinks && (
                    <Icon
                        size={14}
                        className={classes.chevron}
                        icon={"ic:round-chevron-right"}
                        color={theme.colors.grayscale[9]}
                        style={{ transform: opened ? `rotate(-90deg)` : "rotate(90deg)" }}
                    />
                )}
            </Group>
        </UnstyledButton>
    );

    // Group
    return (
        <React.Fragment>
            {hasLinks || soon ? <React.Fragment>{control}</React.Fragment> : <Link to={link}>{control}</Link>}
            {hasLinks && <Collapse in={opened}>{items}</Collapse>}
        </React.Fragment>
    );
}
