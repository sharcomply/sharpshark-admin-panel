import styled from "@emotion/styled";
import { Group } from "@mantine/core";
import PageTitle from "components/PageTitle";
import { TabLink } from "components/TabLink";
import { Outlet } from "react-router-dom";

const DisputesList = () => {
    return (
        <Root>
            <PageTitle title={"Dispute resolution"} />
            <Group position={"apart"} spacing={16}>
                <Group spacing={12}>
                    <TabLink variant="outlined" to="/disputes/draft" label="Draft" />
                    <TabLink variant="outlined" to="/disputes/in-progress" label="In progress" />
                    <TabLink variant="outlined" to="/disputes/claim-sent" label="Claim Sent" />
                    <TabLink variant="outlined" to="/disputes/ended" label="Ended" />
                </Group>
            </Group>

            <Outlet />
        </Root>
    );
};

export default DisputesList;

const Root = styled.div`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    margin: 40px;
    padding: 20px;
    margin-top: 65px;
`;
