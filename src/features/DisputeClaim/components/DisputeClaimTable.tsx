import {Pagination, Stack, Text, TextInput} from "@mantine/core";
import styled from "@emotion/styled";
import {Fragment, useCallback, useEffect, useState} from "react";
import {TableHead} from "components/TableHead";
import {useDisputesClaim} from "../hooks/useDisputesClaim";
import {DisputeClaimItem} from "./DisputeClaimItem";
import {sortsTypes} from "../../../helpers/sortConstants";


const defaultSorts = [[]]
// Component
export default function DisputeClaimTable() {
    const [groupAlerts, setGroupAlerts] = useState<Disput[] | undefined>([]);
    const [search, setSearch] = useState("")
    const [sortOptions, setSortOptions] = useState(defaultSorts)
    const [currentPage, setCurrentPage] = useState(1);

    let status: string;
    switch (window.location.pathname){
        case "/disputes/claim-sent-website-owner":
            status = "Claim Sent: Website Owner"
            break;
        case "/disputes/claim-sent-provider":
            status = "Claim Sent: Provider"
            break;
        case "/disputes/claim-sent-google":
            status = "Claim Sent: Google"
            break;
        default:
            status = "Claim Sent: Website Owner";
    }
    
    const {disputesList, pages, refetch} = useDisputesClaim(search, sortOptions, currentPage-1, status);

    const handleSearch = useCallback(async (value: string) => {
        setSearch(value)
        setCurrentPage(1)
    }, [setSearch, setCurrentPage])

    const handleChangeSort = useCallback(async (values: any) => {
        setSortOptions(values)
        setGroupAlerts(groupByDocumentId(disputesList?.items!));
    }, [setSortOptions])


    function groupByDocumentId(arr: any) {
        const temp = arr?.reduce((acc: any, elem: any) => {
            const documentId = elem.documentId._id;

            if (!acc[documentId]) {
                acc[documentId] = [];
            }

            acc[documentId].push(elem);
            return acc;
        }, {});
        if (temp === undefined) return;
        return Object.getOwnPropertyNames(temp)?.map((k) => temp[k]);
    }

    useEffect(() => {
        setGroupAlerts(groupByDocumentId(disputesList?.items!));
    }, [disputesList, setGroupAlerts]);


    return (
        <Root>
            <Stack align={'flex-end'} mt={8} mr={8}>
                <TextInput placeholder={"Search"} value={search} onChange={(value) => handleSearch(value.target.value)}/>
            </Stack>
            <Table>
                <TableHead
                    items={[
                        {title: "title", field: 'documentId.title', value: false, sortType: sortsTypes.string, width: "40%"},
                        {title: "appeared_on", field: "appearedOn", value: false, sortType: sortsTypes.string, width: "20%"},
                        {title: "discovered", field: 'createdAt', sortType: sortsTypes.date, value: false, width: "20%"},
                        {title: "", width: "11%"},
                        {title: "", width: "8%"},
                        {title: "", width: "8%"},
                        {title: "", width: "5%"},
                    ]}
                    change={handleChangeSort}
                />

                <tbody>
                {!!disputesList?.items.length && (
                    <>
                        <Fragment>
                            {groupAlerts?.map((item: any) => (
                                    <DisputeClaimItem key={item[0]._id} item={item} refetch={refetch}/>
                                ))}
                        </Fragment>
                    </>
                )}

                {!disputesList?.items.length && (
                    <tr style={{borderTop: "1px solid #DDDEEE"}}>
                        <td style={{padding: 16}}>
                            <Text size={"sm"} color={"#666892"} weight={"400"}>
                                This is a blank page... Cases will appear here
                            </Text>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                )}
                </tbody>
            </Table>
            {pages > 1 && (
                <Stack align={"center"} style={{marginBlock: "16px"}}>
                    <Pagination style={{margin: "0 auto;"}} size="lg" page={currentPage} onChange={setCurrentPage} total={pages}/>
                </Stack>
            )}
        </Root>
    );
}

// Styling
const Root = styled(Stack)`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    margin-top: 20px;
`;

export const Table = styled.table`
    width: 100%;
    table-layout: fixed;
    border-collapse: collapse;
    overflow-wrap: anywhere;
`;
