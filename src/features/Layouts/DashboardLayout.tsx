import { Group, Stack } from "@mantine/core";
import { Sidebar } from "features/Sidebar";
import { Outlet } from "react-router-dom";

export function DashboardLayout() {
    return (
        <Group spacing={0} align={"flex-start"}>
            <Sidebar />
            <Stack sx={{ flex: 1 }}>
                <Outlet />
            </Stack>
        </Group>
    );
}
