import { Center, Stack } from "@mantine/core";
import { Outlet } from "react-router-dom";

export function AuthLayout() {
    return (
        <Center sx={{ flex: 1, minHeight: "100vh" }}>
            <Stack spacing={0} p={20}>
                <Outlet />
            </Stack>
        </Center>
    );
}
