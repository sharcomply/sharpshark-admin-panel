import styled from "@emotion/styled";
import { Group } from "@mantine/core";
import PageTitle from "components/PageTitle";
import { TabLink } from "components/TabLink";
import { Outlet } from "react-router-dom";
import { useDisputeDraftList } from "../DisputeDraft/hooks/useDisputesDraft";
import { useDisputeProgress } from "../DistuteProgress/hooks/useDisputeProgress";
import { useDisputesClaim } from "../DisputeClaim/hooks/useDisputesClaim";
import { useDisputesLetter } from "../DisputeLetterSent/hooks/useDisputesLetter";
import { useDisputeEnded } from "../DisputeEnded/hooks/useDisputeEnded";

const DisputesList = () => {
    const { count: DraftWebsiteOwnerCount } = useDisputeDraftList("",[[]],0,"Draft: Website Owner");
    const { count: DraftProviderCount } = useDisputeDraftList("",[[]],0,"Draft: Provider");
    const { count: DraftGoogleCount } = useDisputeDraftList("",[[]],0,"Draft: Google");
    const { count: InProgressWebsiteOwnerCount } = useDisputeProgress("",[[]],0, "In Progress: Website Owner");
    const { count: InProgressProviderCount } = useDisputeProgress("",[[]],0, "In Progress: Provider");
    const { count: InProgressGoogleCount } = useDisputeProgress("",[[]],0, "In Progress: Google");
    const { count: ClaimSentWebsiteOwnerCount } = useDisputesClaim("",[[]],0, "Claim Sent: Website Owner");
    const { count: ClaimSentProviderCount } = useDisputesClaim("",[[]],0, "Claim Sent: Provider");
    const { count: ClaimSentGoogleCount } = useDisputesClaim("",[[]],0, "Claim Sent: Google");
    const { count: LetterWebsiteOwnerCount } = useDisputesLetter("",[[]],0, "Letter Sent: Website Owner");
    const { count: LetterProviderCount } = useDisputesLetter("",[[]],0, "Letter Sent: Provider");
    const { count: LetterGoogleCount } = useDisputesLetter("",[[]],0, "Letter Sent: Google");
    const { count: ClosedCount } = useDisputeEnded("",[[]],0);

    return (
        <Root>
            <PageTitle title={"Dispute resolution"} />
            <Group position={"apart"} spacing={16}>
                <Group spacing={12}>
                    Website owner level:
                    <TabLink variant="outlined" to="/disputes/draft-website-owner" label={`draft (${DraftWebsiteOwnerCount || 0})`} />
                    <TabLink variant="outlined" to="/disputes/in-progress-website-owner" label={`in progress (${InProgressWebsiteOwnerCount || 0})`} />
                    <TabLink variant="outlined" to="/disputes/claim-sent-website-owner" label={`claim sent (${ClaimSentWebsiteOwnerCount || 0})`} />
                    <TabLink variant="outlined" to="/disputes/letter-sent-website-owner" label={`claiming process finished waits for decision (${LetterWebsiteOwnerCount || 0})`} />
                </Group>
                <Group spacing={12}>
                    Provider level:
                    <TabLink variant="outlined" to="/disputes/draft-provider" label={`draft (${DraftProviderCount || 0})`} />
                    <TabLink variant="outlined" to="/disputes/in-progress-provider" label={`in progress (${InProgressProviderCount || 0})`} />
                    <TabLink variant="outlined" to="/disputes/claim-sent-provider" label={`claim sent (${ClaimSentProviderCount || 0})`}  />
                    <TabLink variant="outlined" to="/disputes/letter-sent-provider" label={`claiming process finished waits for decision (${LetterProviderCount || 0})`} />
                </Group>
                <Group spacing={12}>
                    Google level:
                    <TabLink variant="outlined" to="/disputes/draft-google" label={`draft (${DraftGoogleCount || 0})`} />
                    <TabLink variant="outlined" to="/disputes/in-progress-google" label={`in progress (${InProgressGoogleCount || 0})`} />
                    <TabLink variant="outlined" to="/disputes/claim-sent-google" label={`claim sent (${ClaimSentGoogleCount || 0})`}  />
                    <TabLink variant="outlined" to="/disputes/letter-sent-google" label={`claiming process finished waits for decision (${LetterGoogleCount || 0})`} />
                    <TabLink variant="outlined" to="/disputes/ended" label={`closed (${ClosedCount || 0})`}/>
                </Group>
            </Group>

            <Outlet />
        </Root>
    );
};

export default DisputesList;

const Root = styled.div`
    background-color: #ffffff;
    border: 1px solid #dddeee;
    border-radius: 8px;
    margin: 40px;
    padding: 20px;
    margin-top: 65px;
`;
