import React from "react";
import LocaleModals from "./containers/LocaleModals";

// Component
export default function ModalContainer() {

    return (
        <React.Fragment>
            <LocaleModals />
        </React.Fragment>
    );
}
