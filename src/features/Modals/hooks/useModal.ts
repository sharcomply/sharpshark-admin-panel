import { AppState } from "store";
import { useDispatch, useSelector } from "react-redux";
import { useCallback, useMemo } from "react";
import modalSlice from "../store";
import { ModalContent, ModalType } from "../interfaces";

export const useModal = (watchType?: ModalType) => {
    const state = useSelector((state: AppState) => state.modals);
    const dispatch = useDispatch();

    const opened = useMemo(() => {
        return state.open && state.type === watchType;
    }, [ watchType, state.open, state.type ]);

    // Actions
    const handleOpen = useCallback((payload: { type: ModalType; content: ModalContent; }) => {
        dispatch(modalSlice.actions.open(payload));
    }, [ dispatch ]);

    const handleSwitch = useCallback((content: ModalContent) => {
        dispatch(modalSlice.actions.switch(content));
    }, [ dispatch ]);

    const handleClose = useCallback(() => {
        dispatch(modalSlice.actions.close());
    }, [ dispatch ]);

    return {
        state: {
            ...state,
            opened,
        },
        actions: {
            open: handleOpen,
            close: handleClose,
            switch: handleSwitch
        }
    };
};