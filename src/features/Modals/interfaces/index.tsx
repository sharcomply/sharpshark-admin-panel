export enum ModalType {
    LOCALE
}
export enum ModalContent {
    LOCALE_DELETE,
    LOCALE_CREATE
}
