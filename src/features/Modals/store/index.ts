import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ModalContent, ModalType } from "../interfaces";

export interface ModalState {
    open: boolean;
    type: ModalType | null;
    content: ModalContent | null;
}

const initialState: ModalState = {
    open: false,
    type: null,
    content: null,
};

const slice = createSlice({
    name: "modals",
    initialState,
    reducers: {
        open: (_, action: PayloadAction<{ type: ModalType; content: ModalContent }>) => ({
            open: true,
            type: action.payload.type,
            content: action.payload.content,
        }),
        switch: (state, action: PayloadAction<ModalContent>) => ({
            ...state,
            content: action.payload,
        }),
        close: (state) => ({
            ...state,
            open: false,
        }),
    },
});

export default slice
