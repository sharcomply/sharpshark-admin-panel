import {ActionIcon, createStyles, Group, Input, InputWrapper, Stack, Text} from "@mantine/core";
import * as Yup from 'yup';
import {Button} from "../../../components/Button";
import {useCallback, useEffect} from "react";
import {useLocaleMutation} from "../../Locale/hooks/useLocaleMutation";
import {Icon} from "../../../components";
import {ModalType} from "../interfaces";
import {useModal} from "../hooks/useModal";
import {yupResolver} from "@hookform/resolvers/yup";
import {useForm, Controller} from "react-hook-form";

const schema = Yup.object().shape({
    name: Yup.string().min(4, 'Name should have at least 4 letters'),
});


const useStyles = createStyles(() => ({
    root: {
        position: 'relative',
        width: 350
    },
}))

export const LocaleCreate = () => {

    const modal = useModal(ModalType.LOCALE)

    const form = useForm({
        resolver: yupResolver(schema),
        defaultValues: {
            name: '',
        },
    });

    useEffect(() => {
        console.log(form.formState.errors)
    }, [form])

    const {handleCreate, createLoading} = useLocaleMutation()

    const {classes} = useStyles()

    const onSubmit = useCallback(async (values: any) => {
        await handleCreate(values.name)
        modal.actions.close()
    }, [handleCreate, modal ])

    return (
        <form onSubmit={form.handleSubmit(onSubmit)}>
        <Stack className={classes.root}>
            <Group position={'apart'} align={'center'}>
                <Group position={"left"}>
                    <Text size={'md'} weight={'bold'}>
                        Create new locale
                    </Text>
                </Group>
                <Group position={'right'}>
                    <ActionIcon onClick={modal.actions.close} variant={'transparent'}>
                        <Icon color={'red'} icon="ep:close"/>
                    </ActionIcon>
                </Group>
            </Group>
            <Stack>
                <Controller
                    name={"name"}
                    control={form.control}
                    render={({ field, fieldState }) => (
                        <InputWrapper
                            id="Name"
                            required
                            label="Name"
                            description={'For example: English, Spanish'}
                            error={fieldState.error?.message}
                        >
                            <Input placeholder="English" {...field} />
                        </InputWrapper>

                    )}
                />
                <Button loading={createLoading} width={"100%"} height={'36px'} variant={'filled'} color={'primary'}>
                    Save
                </Button>
            </Stack>
        </Stack>
        </form>
    )
}


