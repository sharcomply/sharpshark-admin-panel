import {ActionIcon, createStyles, Group, Stack, Text} from "@mantine/core";
import {Button} from "components/Button";
import {useLocaleMutation} from "features/Locale/hooks/useLocaleMutation";
import {useCallback} from "react";
import {useLocaleStore} from "features/Locale/hooks/useLocaleStore";
import {Icon} from "../../../components";
import {ModalType} from "../interfaces";
import {useModal} from "../hooks/useModal";

const useStyles = createStyles(() => ({
    root: {
        position: 'relative',
        width: 350
    }
}))

export const LocaleDelete = () => {
    const {actions, state} = useLocaleStore()
    const {handleRemove, removeLoading} = useLocaleMutation()
    const modal = useModal(ModalType.LOCALE)

    const {classes} = useStyles()

    const remove = useCallback(async () => {
        if (!state.remove.id) return

        await handleRemove(state.remove.id, state.remove.name)
        modal.actions.close();
        actions.setRemovePayload({ id: undefined, name: undefined })
    }, [actions, handleRemove, state.remove.id])


    return (
        <Stack className={classes.root}>
            <Group position={'apart'} align={'center'}>
                <Group position={"left"}>
                    <Text size={'md'} weight={'bold'}>
                        Delete Locale: {state.remove.name}
                    </Text>
                </Group>
                <Group position={'right'}>
                    <ActionIcon onClick={modal.actions.close} variant={'transparent'}>
                        <Icon color={'red'} icon="ep:close" />
                    </ActionIcon>
                </Group>
            </Group>

            <Stack>
                <Text align={"center"}>Please confirm your action. Selected locale will be delete!</Text>

                <Button height={36} loading={removeLoading} onClick={() => remove()} variant={'filled'} color={"primary"}>
                    Confirm
                </Button>
            </Stack>
        </Stack>
    )
}