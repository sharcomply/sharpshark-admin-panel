import {Modal} from "@mantine/core";
import {ModalContent, ModalType} from "../interfaces";

import {useModal} from "../hooks/useModal";
import {LocaleCreate} from "../components/LocaleCreate";
import {LocaleDelete} from "../components/LocaleDelete";


export default function LocaleModals() {
    const modal = useModal(ModalType.LOCALE);

    return (
        <Modal
            opened={modal.state.opened}
            onClose={modal.actions.close}
            withCloseButton={false}
            overlayOpacity={0.5}
            transition="slide-down"
            transitionDuration={400}
            closeOnClickOutside={false}
            padding={30}
            centered
            size={"auto"}
        >
            {modal.state.content === ModalContent.LOCALE_DELETE && <LocaleDelete />}
            {modal.state.content === ModalContent.LOCALE_CREATE && <LocaleCreate />}
        </Modal>
    );
}
